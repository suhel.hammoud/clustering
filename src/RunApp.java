import weka.gui.explorer.*;

import java.util.logging.Logger;

public class RunApp {
  static Logger log = Logger.getLogger(RunApp.class.getName());



  public static void main(String[] args) {
    log.info("OK");

    Explorer.main(new String[]{"data/iris.arff"});
  }
}