package weka.clusterers;

import weka.core.Instance;
import weka.core.Instances;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * https://nlp.stanford.edu/IR-book/html/htmledition/evaluation-of-clustering-1.html
 * https://stats.stackexchange.com/questions/15158/precision-and-recall-for-clustering
 */
public class OPairBasedEvaluation {
  final Map<Integer, Integer> labels;
  final int numLabels;

  private int tp, tn, fp, fn;

  /**
   * label attribute should be set before calling the constructor
   *
   * @param data
   */
  public static OPairBasedEvaluation of(Instances data) {
    data.setClassIndex(data.numAttributes() - 1);
    HashMap<Integer, Integer> m = new HashMap<>(data.numInstances());
    IntStream.range(0, data.numInstances())
        .forEach(i -> {
          Instance inst = data.instance(i);
          m.put(i, (int) inst.classValue());
        });
    int numLabels = (int) m.values()
        .stream()
        .distinct()
        .count();
    return new OPairBasedEvaluation(m, numLabels);
  }

  public static OPairBasedEvaluation of(Map<Integer, Integer> labels, int numLabels) {
    return new OPairBasedEvaluation(new HashMap<>(labels), numLabels);
  }

  private OPairBasedEvaluation(Map<Integer, Integer> labels, int numLabels) {
    this.labels = labels;
    this.numLabels = numLabels;
  }


  public static int twoCombinations(int k) {
    if (k < 3) return 1;
    return k * (k - 1) / 2;
  }

  public static int countCalcOneFP(int[] freqs) {
    return Combinations.of(2, freqs)
        .stream()
        .mapToInt(i -> i.get(0) * i.get(1))
        .sum();
  }

  /**
   * return count streamOf labels in data in the same order streamOf attributes
   *
   * @param clusterPoints
   * @param numLabels
   * @return
   */
  public static int[] countLabels(Instances clusterPoints, final int numLabels) {
    clusterPoints.setClassIndex(clusterPoints.numAttributes() - 1);
    //assert classIndex is set before calling
    Map<Integer, Long> counts = clusterPoints.stream()
        .collect(Collectors.groupingBy(i -> (int) i.classValue(), Collectors.counting()));

    return IntStream.range(0, numLabels)
        .map(i -> (counts.get(i)) == null ? 0 : counts.get(i).intValue())
        .toArray();
  }

  private int[] countLabels(List<Integer> points) {
    //assert classIndex is set before calling
    Map<Integer, Long> counts = points.stream()
        .collect(Collectors.groupingBy(
            i -> labels.get(i),
            Collectors.counting()));

    return IntStream.range(0, numLabels)
        .map(i -> (counts.get(i)) == null ? 0 : counts.get(i).intValue())
        .toArray();
  }

  public void calc(List<int[]> cLabels) {
    int numPoints = cLabels.stream()
        .flatMapToInt(i -> Arrays.stream(i))
        .sum();
    calc(cLabels, numPoints);
  }

  public void calc(List<int[]> cLabels, int numPoints) {
    //All pairs
    int allPairs = twoCombinations(numPoints);
//    System.out.println("allPairs = " + allPairs);


    /** All positives (TP + FP) */
    int tpFp = cLabels.stream()
        .mapToInt(i -> twoCombinations(IntStream.of(i).sum()))
        .sum();
//    System.out.println("tpFp = " + tpFp);

    /** calculate TP */

    tp = cLabels.stream()
        .flatMapToInt(i -> IntStream.of(i)
            .filter(v -> v >= 2)
            .map(OPairBasedEvaluation::twoCombinations))
        .sum();
//    System.out.println("tp = " + tp);

    /** calculate FP */
/*

    //method 1 : FP = ALL_TP_FP - TP
    fp = tpFp - tp;
    System.out.println("fp1 = " + fp);
*/

    //method 2
    fp = cLabels.stream()
        .mapToInt(i -> countCalcOneFP(i))
        .sum();
//    System.out.println("fp2 = " + fp);
    assert tpFp == tp + fp;

    /**  All negatives (TN + FN) */
    int tnFn = allPairs - tpFp;
//    System.out.println("tnFn = " + tnFn);


    /** calculate TN */
//    tn = tnFn - fn;
//    System.out.println("tn1 = " + tn);
    tn = countTN(cLabels, numLabels);
//    System.out.println("tn2 = " + tn);

    /** calculate FN */

    //method 1
    fn = tnFn - tn;
//    System.out.println("fn1 = " + fn);

    //method 2

    /*//labelsClustersCounts
    List<int[]> labelFreqs = BUtils.transposeAndFilterZero(cLabels);
    fn = labelFreqs.stream()
        .mapToInt(i -> BUtils.combinProduct(i))
        .sum();
    System.out.println("fn2 = " + fn);
    */

  }

  /**
   * Map<cluster , list of points>
   *
   * @param clusters
   */
  public void calc(Map<Integer, List<Integer>> clusters) {

    int numPoints = (int) clusters.values()
        .stream()
        .flatMap(i -> i.stream())
        .distinct()
        .count();

//    System.out.println("numPoints = " + numPoints);


    //count all labels in each cluster
    List<int[]> cLabels = clusters.values()
        .stream()
        .map(insts -> countLabels(insts))
        .collect(Collectors.toList());

    calc(cLabels, numPoints);

  }

  /**
   * @param length ex 3
   * @return { [0,1], [0,2], [1,2]}
   */
  private static List<int[]> pairs(int length) {
    assert length >= 2;
    if (length == 2) {
      return new ArrayList<>(Arrays.asList(new int[]{0, 1}));
    }
    List<int[]> result = new ArrayList<>(length * length / 2 - length);
    for (int i = 0; i < length - 1; i++) {
      for (int j = i + 1; j < length; j++) {
        result.add(new int[]{i, j});
      }
    }
    return result;
  }

  public static int countTN(List<int[]> cLabels, int numLabels) {
    List<int[]> pairs = pairs(numLabels);

    List<int[]> communities = pairs(cLabels.size());

    int result = 0;
    for (int[] cPair : communities) {
      int[] a = cLabels.get(cPair[0]);
      int[] b = cLabels.get(cPair[1]);
      for (int[] pair : pairs) {
        result += a[pair[0]] * b[pair[1]];
        result += a[pair[1]] * b[pair[0]];
      }
    }
    return result;
  }

  /**
   * @param m_ClusterPoints
   * @param allPoints
   */
  public void calc(Instances[] m_ClusterPoints, int allPoints) {

    //assume the label is the last attribute
    int numPoints = allPoints > 0 ? allPoints :
        Arrays.stream(m_ClusterPoints)
            .mapToInt(Instances::numInstances)
            .sum();
    //TODO assert numPoint == data.numInstances if no outlier points where removed

    //count all labels in each cluster
    List<int[]> clustersLabelsCounts = Arrays.stream(m_ClusterPoints)
        .map(i -> countLabels(i, numLabels))
        .collect(Collectors.toList());

    calc(clustersLabelsCounts, numPoints);
  }

  public int getTp() {
    return tp;
  }

  public int getTn() {
    return tn;
  }

  public int getFp() {
    return fp;
  }

  public int getFn() {
    return fn;
  }

  public double precision() {
    return (double) tp / (tp + fp);
  }

  public double recall() {
    return (double) tp / (tp + fn);
  }

  public double fMeasure() {
    double precision = precision();
    double recall = recall();
    return 2 * recall * precision / (recall + precision);
  }

  public String getResults() {
    StringJoiner s = new StringJoiner("\n");
    s.add(String.format("TP = %6d", getTp()));
    s.add(String.format("FP = %6d", getFp()));
    s.add(String.format("TN = %6d", getTn()));
    s.add(String.format("FN = %6d", getFn()));
    s.add(String.format("Recall = %1.4f", recall()));
    s.add(String.format("Precision = %1.4f", precision()));
    s.add(String.format("F-Measure = %1.4f", fMeasure()));
    return s.toString();
  }

  public static void testPairBasedEval1() {


    Instances data = BUtils.instancesOf("data/iris.arff");
    data.setClassIndex(data.numAttributes() - 1);

    Instance x = data.stream()
        .filter(i -> i.classValue() == 0.0)
        .findFirst()
        .get(); // x

    Instance o = data.stream()
        .filter(i -> i.classValue() == 1.0)
        .findFirst()
        .get(); // o

    Instance v = data.stream()
        .filter(i -> i.classValue() == 2.0)
        .findFirst()
        .get(); // <>

    data.clear();

    data.add(x);//0, 0
    data.add(x);//1, 0
    data.add(x);//2, 0
    data.add(x);//3, 0
    data.add(x);//4, 0
    data.add(x);//5, 1
    data.add(x);//6, 2
    data.add(x);//7, 2

    data.add(o);//8, 0
    data.add(o);//9, 1
    data.add(o);//10, 1
    data.add(o);//11, 1
    data.add(o);//12, 1

    data.add(v);//13, 1
    data.add(v);//14, 2
    data.add(v);//15, 2
    data.add(v);//16, 2

    Map<Integer, List<Integer>> clusters = new LinkedHashMap<>();
    clusters.put(0, Arrays.asList(0, 1, 2, 3, 4, 8));
    clusters.put(1, Arrays.asList(5, 9, 10, 11, 12, 13));
    clusters.put(2, Arrays.asList(6, 7, 14, 15, 16));

    // 1- Init eval with the instance reference
    OPairBasedEvaluation eval = OPairBasedEvaluation.of(data);

    // 2- Call "calc" method passing the current points clustered in an array streamOf instances
    eval.calc(clusters);

    // 3- Print out the current results
    System.out.println("TP = " + eval.getTp());
    System.out.println("FP = " + eval.getFp());
    System.out.println("TN = " + eval.getTn());
    System.out.println("FN = " + eval.getFn());

    System.out.println("recall = " + eval.recall());       // 0.45454
    System.out.println("precision = " + eval.precision()); // 0.5
    System.out.println("f-measure = " + eval.fMeasure()); //

    // 4- Repeat steps 2 and 3 for each new clusters
  }

  public static void testPairBasedEval2() {
    Instances data = BUtils.instancesOf("data/iris.arff");
    data.setClassIndex(data.numAttributes() - 1);

    Instance x = data.stream()
        .filter(i -> i.classValue() == 0.0)
        .findFirst()
        .get(); // x

    Instance o = data.stream()
        .filter(i -> i.classValue() == 1.0)
        .findFirst()
        .get(); // o

    Instance v = data.stream()
        .filter(i -> i.classValue() == 2.0)
        .findFirst()
        .get(); // <>

    Instances[] clusters = new Instances[3];
    for (int i = 0; i < clusters.length; i++) {
      clusters[i] = new Instances(data, 0);
    }

    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(o);
    clusters[0].add(v);

    clusters[1].add(x);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);

    clusters[2].add(o);
    clusters[2].add(o);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);


    // 1- Init eval with the instance reference
    OPairBasedEvaluation eval = OPairBasedEvaluation.of(data);

    // 2- Call "calc" method passing the current points clustered in an array streamOf instances
    eval.calc(clusters, 20);

    // 3- Print out the current results
    System.out.println("TP = " + eval.getTp());
    System.out.println("FP = " + eval.getFp());
    System.out.println("TN = " + eval.getTn());
    System.out.println("FN = " + eval.getFn());

    System.out.println("recall = " + eval.recall());       // 0.52
    System.out.println("precision = " + eval.precision()); // 0.56

    // 4- Repeat steps 2 and 3 for each new clusters
  }

  public static void testPairBasedEval3() {
    Instances data = BUtils.instancesOf("data/iris.arff");
    data.setClassIndex(data.numAttributes() - 1);

    Instance x = data.stream()
        .filter(i -> i.classValue() == 0.0)
        .findFirst()
        .get(); // x

    Instance o = data.stream()
        .filter(i -> i.classValue() == 1.0)
        .findFirst()
        .get(); // o

    Instance v = data.stream()
        .filter(i -> i.classValue() == 2.0)
        .findFirst()
        .get(); // <>

    Instances[] clusters = new Instances[3];
    for (int i = 0; i < clusters.length; i++) {
      clusters[i] = new Instances(data, 0);
    }

    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(o);
    clusters[0].add(o);
    clusters[0].add(v);
    clusters[0].add(v);
    clusters[0].add(v);
    clusters[0].add(v);

    clusters[1].add(x);
    clusters[1].add(x);
    clusters[1].add(x);
    clusters[1].add(x);
    clusters[1].add(o);
    clusters[1].add(v);
    clusters[1].add(v);
    clusters[1].add(v);

    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(o);
    clusters[2].add(o);
    clusters[2].add(o);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);


    // 1- Init eval with the instance reference
    OPairBasedEvaluation eval = OPairBasedEvaluation.of(data);

    // 2- Call "calc" method passing the current points clustered in an array of instances
    eval.calc(clusters, 28);

    // 3- Print out the current results
    System.out.println("TP = " + eval.getTp());
    System.out.println("FP = " + eval.getFp());
    System.out.println("TN = " + eval.getTn());
    System.out.println("FN = " + eval.getFn());

    System.out.println("recall = " + eval.recall());       // 0.52
    System.out.println("precision = " + eval.precision()); // 0.56
    System.out.println("f-measure = " + eval.fMeasure()); // 0.56

    // 4- Repeat steps 2 and 3 for each new clusters
  }


  public static void main(String[] args) {
    testPairBasedEval2();
  }

}
