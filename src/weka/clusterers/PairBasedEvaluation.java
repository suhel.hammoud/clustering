package weka.clusterers;

import weka.core.Instance;
import weka.core.Instances;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * https://nlp.stanford.edu/IR-book/html/htmledition/evaluation-of-clustering-1.html
 * https://stats.stackexchange.com/questions/15158/precision-and-recall-for-clustering
 */
public class PairBasedEvaluation {
  final Instances data;

  private int tp, tn, fp, fn;

  public PairBasedEvaluation(Instances data) {
    this.data = new Instances(data);
  }

  public static int combineTwo(int k) {
    if (k < 3) return 1;
    return k * (k - 1) / 2;
  }

  public static double calcOneFP(int[] freqs) {
    return Combinations.of(2, freqs)
        .stream()
        .mapToInt(i -> i.get(0) * i.get(1))
        .sum();
  }

  /**
   * return count streamOf labels in data in the same order streamOf attributes
   *
   * @param clusterPoints
   * @param numLabels
   * @return
   */
  public static int[] countLabels(Instances clusterPoints, final int numLabels) {
    //assert classIndex is set before calling
    Map<Integer, Long> counts = clusterPoints.stream()
        .collect(Collectors.groupingBy(i -> (int) i.classValue(), Collectors.counting()));

    return IntStream.range(0, numLabels)
        .map(i -> (counts.get(i)) == null ? 0 : counts.get(i).intValue())
        .toArray();
  }

  private int[] countLabels(List<Integer> points) {
    //assert classIndex is set before calling
    Map<Integer, Long> counts = points.stream()
        .collect(Collectors.groupingBy(
            i -> (int) data.instance(i).classValue(),
            Collectors.counting()));

    return IntStream.range(0, data.classAttribute().numValues())
        .map(i -> (counts.get(i)) == null ? 0 : counts.get(i).intValue())
        .toArray();
  }


  public void calc(Map<Integer, List<Integer>> clusters) {
    //assert data data are streamOf the same as original
    data.setClassIndex(data.numAttributes() - 1);
    final int numLabels = data.classAttribute().numValues();

    //assume the label is the last attribute
    int numPoints = clusters.values()
        .stream()
        .mapToInt(List::size)
        .sum();
//    System.out.println("numPoints = " + numPoints);
    //TODO assert numPoint == data.numInstances if no outlier points where removed
    //All pairs
    int allPairs = combineTwo(numPoints);
//    System.out.println("allPairs = " + allPairs);

    //count all labels in each cluster
    List<int[]> clustersLabelsCounts = clusters.values()
        .stream()
        .map(insts -> countLabels(insts))
        .collect(Collectors.toList());

//    System.out.println("ClustersLabelsCounts");
//    clustersLabelsCounts.forEach(i -> System.out.println(Arrays.toString(i)));

    //All positives (TP + FP)
    int tpFp = clustersLabelsCounts.stream()
        .mapToInt(i -> combineTwo(IntStream.of(i).sum()))
        .sum();
//    System.out.println("tpFp = " + tpFp);

    // TP
    tp = clustersLabelsCounts.stream()
        .flatMapToInt(i -> IntStream.of(i)
            .filter(v -> v >= 2)
            .map(PairBasedEvaluation::combineTwo))
        .sum();
//    System.out.println("tp = " + tp);

    //FP = ALL_TP_FP - TP
    fp = tpFp - tp;

    fp = clustersLabelsCounts.stream()
        .mapToInt(i -> (int) calcOneFP(i))
        .sum();

    //  All negatives (TN + FN) = All_pairs - All Positives (TP + FP)
    int tnFn = allPairs - tpFp;
//    System.out.println("tnFn = " + tnFn);

    //labelsClustersCounts
    List<int[]> labelFreqs = BUtils.transposeAndFilterZero(clustersLabelsCounts);

    // FN
    fn = labelFreqs.stream()
        .mapToInt(i -> BUtils.combinProduct(i))
        .sum();
//    System.out.println("fn = " + fn);

    // TN = All Negatives - FN
    tn = tnFn - fn;
//    System.out.println("tn = " + tn);
  }

  public void calc(Instances[] m_ClusterPoints) {
    //assert data data are streamOf the same as original
    data.setClassIndex(data.numAttributes() - 1);
    final int numLabels = data.classAttribute().numValues();

    //assume the label is the last attribute
    int numPoints = Arrays.stream(m_ClusterPoints)
        .mapToInt(Instances::numInstances)
        .sum();
//    System.out.println("numPoints = " + numPoints);
    //TODO assert numPoint == data.numInstances if no outlier points where removed
    //All pairs
    int allPairs = combineTwo(numPoints);
//    System.out.println("allPairs = " + allPairs);

    //count all labels in each cluster
    List<int[]> clustersLabelsCounts = Arrays.stream(m_ClusterPoints)
        .map(insts -> countLabels(insts, numLabels))
        .collect(Collectors.toList());

//    System.out.println("ClustersLabelsCounts");
//    clustersLabelsCounts.forEach(i -> System.out.println(Arrays.toString(i)));

    //All positives (TP + FP)
    int tpFp = clustersLabelsCounts.stream()
        .mapToInt(i -> combineTwo(IntStream.of(i).sum()))
        .sum();
//    System.out.println("tpFp = " + tpFp);

    // TP
    tp = clustersLabelsCounts.stream()
        .flatMapToInt(i -> IntStream.of(i)
            .filter(v -> v >= 2)
            .map(PairBasedEvaluation::combineTwo))
        .sum();
//    System.out.println("tp = " + tp);

    //FP = ALL_TP_FP - TP
//    fp = tpFp - tp;
//    System.out.println("fp = " + fp);

    fp = clustersLabelsCounts.stream()
        .mapToInt(i -> (int) calcOneFP(i))
        .sum();
//    System.out.println("fp 2 = " + fp);

    //  All negatives (TN + FN) = All_pairs - All Positives (TP + FP)
    int tnFn = allPairs - tpFp;
//    System.out.println("tnFn = " + tnFn);

    //labelsClustersCounts
    List<int[]> labelFreqs = BUtils.transposeAndFilterZero(clustersLabelsCounts);

    // FN
    fn = labelFreqs.stream()
        .mapToInt(i -> BUtils.combinProduct(i))
        .sum();
//    System.out.println("fn = " + fn);

    // TN = All Negatives - FN
    tn = tnFn - fn;
//    System.out.println("tn = " + tn);
  }

  public int getTp() {
    return tp;
  }

  public int getTn() {
    return tn;
  }

  public int getFp() {
    return fp;
  }

  public int getFn() {
    return fn;
  }

  public double precision() {
    return (double) tp / (tp + fp);
  }

  public double recall() {
    return (double) tp / (tp + fn);
  }

  public double fMeasure() {
    double precision = precision();
    double recall = recall();
    return 2 * recall * precision / (recall + precision);
  }


  public static void testPairBasedEval1() {
    Instances data = BUtils.instancesOf("data/iris.arff");
    data.setClassIndex(data.numAttributes() - 1);

    Instance x = data.stream()
        .filter(i -> i.classValue() == 0.0)
        .findFirst()
        .get(); // x

    Instance o = data.stream()
        .filter(i -> i.classValue() == 1.0)
        .findFirst()
        .get(); // o

    Instance v = data.stream()
        .filter(i -> i.classValue() == 2.0)
        .findFirst()
        .get(); // <>

    Instances[] clusters = new Instances[3];
    for (int i = 0; i < clusters.length; i++) {
      clusters[i] = new Instances(data, 0);
    }

    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(o);

    clusters[1].add(x);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(v);

    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);

    // 1- Init eval with the instance reference
    PairBasedEvaluation eval = new PairBasedEvaluation(data);

    // 2- Call "calc" method passing the current points clustered in an array streamOf instances
    eval.calc(clusters);

    // 3- Print out the current results
    System.out.println("TP = " + eval.getTp());
    System.out.println("FP = " + eval.getFp());
    System.out.println("TN = " + eval.getTn());
    System.out.println("FN = " + eval.getFn());

    System.out.println("recall = " + eval.recall());       // 0.45454
    System.out.println("precision = " + eval.precision()); // 0.5

    // 4- Repeat steps 2 and 3 for each new clusters
  }

  public static void testPairBasedEval2() {
    Instances data = BUtils.instancesOf("data/iris.arff");
    data.setClassIndex(data.numAttributes() - 1);

    Instance x = data.stream()
        .filter(i -> i.classValue() == 0.0)
        .findFirst()
        .get(); // x

    Instance o = data.stream()
        .filter(i -> i.classValue() == 1.0)
        .findFirst()
        .get(); // o

    Instance v = data.stream()
        .filter(i -> i.classValue() == 2.0)
        .findFirst()
        .get(); // <>

    Instances[] clusters = new Instances[3];
    for (int i = 0; i < clusters.length; i++) {
      clusters[i] = new Instances(data, 0);
    }

    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(o);
    clusters[0].add(v);

    clusters[1].add(x);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);

    clusters[2].add(o);
    clusters[2].add(o);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);


    // 1- Init eval with the instance reference
    PairBasedEvaluation eval = new PairBasedEvaluation(data);

    // 2- Call "calc" method passing the current points clustered in an array streamOf instances
    eval.calc(clusters);

    // 3- Print out the current results
    System.out.println("TP = " + eval.getTp());
    System.out.println("FP = " + eval.getFp());
    System.out.println("TN = " + eval.getTn());
    System.out.println("FN = " + eval.getFn());

    System.out.println("recall = " + eval.recall());       // 0.52
    System.out.println("precision = " + eval.precision()); // 0.56

    // 4- Repeat steps 2 and 3 for each new clusters
  }

  public static void testPairBasedEval3() {
    Instances data = BUtils.instancesOf("data/iris.arff");
    data.setClassIndex(data.numAttributes() - 1);

    Instance x = data.stream()
        .filter(i -> i.classValue() == 0.0)
        .findFirst()
        .get(); // x

    Instance o = data.stream()
        .filter(i -> i.classValue() == 1.0)
        .findFirst()
        .get(); // o

    Instance v = data.stream()
        .filter(i -> i.classValue() == 2.0)
        .findFirst()
        .get(); // <>

    Instances[] clusters = new Instances[3];
    for (int i = 0; i < clusters.length; i++) {
      clusters[i] = new Instances(data, 0);
    }

    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(o);
    clusters[0].add(o);
    clusters[0].add(v);
    clusters[0].add(v);
    clusters[0].add(v);
    clusters[0].add(v);

    clusters[1].add(x);
    clusters[1].add(x);
    clusters[1].add(x);
    clusters[1].add(x);
    clusters[1].add(o);
    clusters[1].add(v);
    clusters[1].add(v);
    clusters[1].add(v);

    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(o);
    clusters[2].add(o);
    clusters[2].add(o);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);


    // 1- Init eval with the instance reference
    PairBasedEvaluation eval = new PairBasedEvaluation(data);

    // 2- Call "calc" method passing the current points clustered in an array of instances
    eval.calc(clusters);

    // 3- Print out the current results
    System.out.println("TP = " + eval.getTp());
    System.out.println("FP = " + eval.getFp());
    System.out.println("TN = " + eval.getTn());
    System.out.println("FN = " + eval.getFn());

    System.out.println("recall    = " + eval.recall());       // 0.52
    System.out.println("precision = " + eval.precision()); // 0.56
    System.out.println("f-measure = " + eval.fMeasure()); // 0.56

    // 4- Repeat steps 2 and 3 for each new clusters
  }

  public String getResults() {
    StringJoiner s = new StringJoiner("\n");
    s.add(String.format("TP = %6d", getTp()));
    s.add(String.format("FP = %6d", getFp()));
    s.add(String.format("TN = %6d", getTn()));
    s.add(String.format("FN = %6d", getFn()));
    s.add(String.format("Recall    = %1.4f", recall()));
    s.add(String.format("Precision = %1.4f", precision()));
    s.add(String.format("F-Measure = %1.4f", fMeasure()));
    return s.toString();
  }

  public static void main(String[] args) {
    testPairBasedEval3();
  }

}
