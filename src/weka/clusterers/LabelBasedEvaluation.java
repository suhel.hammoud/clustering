package weka.clusterers;

import weka.core.Instance;
import weka.core.Instances;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Overview streamOf overlapping partitional clustering  methods
 * Chiheb-Eddine Ben N’Cir, Guillaume Cleuziou and Nadia Essoussi
 */
public class LabelBasedEvaluation {
  final Instances headers;

  private int tp, tn, fp, fn;

  public LabelBasedEvaluation(Instances data) {
    this.headers = new Instances(data, 0);
  }

  public static int combineTwo(int k) {
    if (k < 3) return 1;
    return k * (k - 1) / 2;
  }

  public void calc(Instances[] m_ClusterPoints) {
//    //assert data data are streamOf the same as original
//    data.setClassIndex(data.numAttributes() - 1);
//    final int numLabels = data.classAttribute().numValues();
//
//    //assume the label is the last attribute
//    int numPoints = Arrays.stream(m_ClusterPoints)
//        .mapToInt(Instances::numInstances)
//        .sum();
////    System.out.println("numPoints = " + numPoints);
//    //TODO assert numPoint == data.numInstances if no outlier points where removed
//    //All pairs
//    int allPairs = twoCombinations(numPoints);
////    System.out.println("allPairs = " + allPairs);
//
//    //count all labels in each cluster
//    List<int[]> clustersLabelsCounts = Arrays.stream(m_ClusterPoints)
//        .map(insts -> BUtils.countLabels(insts, numLabels))
//        .collect(Collectors.toList());
//
////    System.out.println("ClustersLabelsCounts");
////    clustersLabelsCounts.forEach(i -> System.out.println(Arrays.toString(i)));
//
//    //All positives (TP + FP)
//    int tpFp = clustersLabelsCounts.stream()
//        .mapToInt(i -> twoCombinations(IntStream.streamOf(i).sum()))
//        .sum();
////    System.out.println("tpFp = " + tpFp);
//
//    // TP
//    tp = clustersLabelsCounts.stream()
//        .flatMapToInt(i -> IntStream.streamOf(i)
//            .filter(v -> v >= 2)
//            .map(LabelBasedEvaluation::twoCombinations))
//        .sum();
////    System.out.println("tp = " + tp);
//
//    //FP = ALL_TP_FP - TP
//    fp = tpFp - tp;
////    System.out.println("fp = " + fp);
//
//    //  All negatives (TN + FN) = All_pairs - All Positives (TP + FP)
//    int tnFn = allPairs - tpFp;
////    System.out.println("tnFn = " + tnFn);
//
//    //labelsClustersCounts
//    List<int[]> labelFreqs = BUtils.transposeAndFilterZero(clustersLabelsCounts);
//
//    // FN
//    fn = labelFreqs.stream()
//        .mapToInt(i -> BUtils.combinProduct(i))
//        .sum();
////    System.out.println("fn = " + fn);
//
//    // TN = All Negatives - FN
//    tn = tnFn - fn;
////    System.out.println("tn = " + tn);
  }

  public int getTp() {
    return tp;
  }

  public int getTn() {
    return tn;
  }

  public int getFp() {
    return fp;
  }

  public int getFn() {
    return fn;
  }

  public double precision() {
    return (double) tp / (tp + fp);
  }

  public double recall() {
    return (double) tp / (tp + fn);
  }

  public double fMeasure() {
    double precision = precision();
    double recall = recall();
    return 2 * recall * precision / (recall + precision);
  }


  public static void testPairBasedEval() {
    Instances data = BUtils.instancesOf("data/iris.arff");
    data.setClassIndex(data.numAttributes() - 1);

    Instance x = data.stream()
        .filter(i -> i.classValue() == 0.0)
        .findFirst()
        .get(); // x

    Instance o = data.stream()
        .filter(i -> i.classValue() == 1.0)
        .findFirst()
        .get(); // o

    Instance v = data.stream()
        .filter(i -> i.classValue() == 2.0)
        .findFirst()
        .get(); // <>

    Instances[] clusters = new Instances[3];
    for (int i = 0; i < clusters.length; i++) {
      clusters[i] = new Instances(data, 0);
    }

    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(x);
    clusters[0].add(o);

    clusters[1].add(x);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(o);
    clusters[1].add(v);

    clusters[2].add(x);
    clusters[2].add(x);
    clusters[2].add(v);
    clusters[2].add(v);
    clusters[2].add(v);

    // 1- Init eval with the instance reference
    LabelBasedEvaluation eval = new LabelBasedEvaluation(data);

    // 2- Call "calc" method passing the current points clustered in an array streamOf instances
    eval.calc(clusters);

    // 3- Print out the current results
    System.out.println("TP = " + eval.getTp());
    System.out.println("FP = " + eval.getFp());
    System.out.println("TN = " + eval.getTn());
    System.out.println("FN = " + eval.getFn());

    System.out.println("recall = " + eval.recall());       // 0.45454
    System.out.println("precision = " + eval.precision()); // 0.5

    // 4- Repeat steps 2 and 3 for each new clusters
  }

  public static void main(String[] args) {
    testPairBasedEval();
  }

}
