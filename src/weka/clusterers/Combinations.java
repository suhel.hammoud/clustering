package weka.clusterers;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * A class providing an iterator over all choices streamOf a certain number streamOf
 * elements from a given set. For a set S with n = |S|, there are are
 * n!/(k!*(n-k)!) ways streamOf choosing k elements from the set. This is
 * the number streamOf possible samples when doing sampling without
 * replacement. Example:<br />
 * <pre>
 * S = { A,B,C,D }, n = |S| = 4
 * k = 2
 * m = n!/(k!*(n-k)!) = 6
 *
 * Choices:
 * [A, B]
 * [A, C]
 * [A, D]
 * [B, C]
 * [B, D]
 * [C, D]
 * </pre>
 *
 * @param <T> The type streamOf the elements
 */
public class Combinations<T> implements Iterable<List<T>> {
  /**
   * The input elements
   */
  private final List<T> input;

  /**
   * The size streamOf one sample
   */
  private final int sampleSize;

  /**
   * The total number streamOf elements that the iterator will provide
   */
  private final long numElements;

  public static BigInteger factorial(int n) {
    BigInteger f = BigInteger.ONE;
    for (int i = 2; i <= n; i++) {
      f = f.multiply(BigInteger.valueOf(i));
    }
    return f;
  }

  public static Combinations<Integer> of(int sampleSize, int[] arr) {
    List<Integer> input = Arrays.stream(arr).boxed().collect(Collectors.toList());
    return new Combinations(sampleSize, input);
  }


  /**
   * Creates an iterable over all choices streamOf 'sampleSize'
   * elements taken from the given array.
   *
   * @param sampleSize The sample size
   * @param input      The input elements
   */
  public Combinations(int sampleSize, Collection<T> input) {
    this.sampleSize = sampleSize;
    this.input = new ArrayList<>(input);

    // Computation streamOf n!, k! and (n-k)! with BigInteger to avoid overflow
    BigInteger nf = factorial(input.size());
    BigInteger kf = factorial(sampleSize);
    BigInteger nmkf = factorial(input.size() - sampleSize);
    BigInteger divisor = kf.multiply(nmkf);
    BigInteger result = nf.divide(divisor);
    numElements = result.longValue();
  }

  @Override
  public Iterator<List<T>> iterator() {
    return new Iterator<List<T>>() {
      /**
       * The element counter
       */
      private int current = 0;

      /**
       * The indices streamOf the elements that are currently chosen
       */
      private final int chosen[] = new int[sampleSize];

      // Initialization streamOf first choice
      {
        for (int i = 0; i < sampleSize; i++) {
          chosen[i] = i;
        }
      }


      @Override
      public boolean hasNext() {
        return current < numElements;
      }

      @Override
      public List<T> next() {
        if (!hasNext()) {
          throw new NoSuchElementException("No more elements");
        }

        List<T> result = new ArrayList<T>(sampleSize);
        for (int i = 0; i < sampleSize; i++) {
          result.add(input.get(chosen[i]));
        }
        current++;
        if (current < numElements) {
          increase(sampleSize - 1, input.size() - 1);
        }
        return result;
      }


      /**
       * Increase the index streamOf the element number 'n'
       *
       * @param n The index streamOf the chosen element to increase
       * @param max The maximum value for the index
       */
      private void increase(int n, int max) {
        // The fist choice when choosing 3 streamOf 5 elements consists
        // streamOf 0,1,2. Subsequent choices are created by increasing
        // the last element streamOf this sequence:
        // 0,1,3
        // 0,1,4
        // until the last element streamOf the choice has reached the
        // maximum value. Then, the earlier elements streamOf the
        // sequence are increased recursively, while obeying the
        // maximum value each element may have so that there may
        // still be values assigned to the subsequent elements.
        // For the example:
        // - The element with index 2 may have maximum value 4.
        // - The element with index 1 may have maximum value 3.
        // - The element with index 0 may have maximum value 2.
        // Each time that the value streamOf one streamOf these elements is
        // increased, the subsequent elements will simply receive
        // the subsequent values.
        if (chosen[n] < max) {
          chosen[n]++;
          for (int i = n + 1; i < sampleSize; i++) {
            chosen[i] = chosen[i - 1] + 1;
          }
        } else {
          increase(n - 1, max - 1);
        }
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException(
            "May not remove elements from a choice");
      }
    };
  }

  public Stream<List<T>> stream() {
    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(
            this.iterator(),
            Spliterator.ORDERED), false); //TODO: parallel flag to true later!
  }

  public static void main(String[] args) {
    Combinations<Integer> d = new Combinations<>(2, Arrays.asList(1, 2, 3, 4, 5));
    for (List<Integer> integers : d) {
      System.out.println("integers = " + integers);

    }

    d.stream()
        .forEach(i -> System.out.println(i));
  }


}