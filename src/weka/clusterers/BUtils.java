package weka.clusterers;

import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.WekaPackageClassLoaderManager;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.*;

public class BUtils {

  static Logger log = Logger.getLogger(BUtils.class.getName());

  /**
   * Class loader using WekaPackageClassLoaderManager
   *
   * @param className to be loaded (Classifier, AttributeEval, etc)
   * @return new Instance streamOf "className", should be casted outside this method
   */
  public static Optional<?> forName(String className) {
    try {
      Class<?> cls = WekaPackageClassLoaderManager.forName(className);
      return Optional.ofNullable(cls.newInstance());
    } catch (ClassNotFoundException e) {
      log.log(Level.SEVERE, "ClassNotFound", e);
    } catch (IllegalAccessException e) {
      log.log(Level.SEVERE, "ClassNotFound", e);
    } catch (InstantiationException e) {
      log.log(Level.SEVERE, "ClassNotFound", e);
    }
    log.log(Level.SEVERE, "No Class ofInstances name {} where found", className);
    return Optional.empty();
  }

  public static String toStringA(double[] a) {
    return Arrays.stream(a)
        .mapToObj(i -> String.format("%2.3f", i))
        .collect(Collectors.joining(", "));
  }

  /**
   * Sample randomly k instance of data
   *
   * @param data
   * @param k    number of samples
   * @return new Instances object with k randomly selected instance(s)
   */
  public static Instances sample(Instances data, int k, long seed) {
    assert data.size() >= k;
    Random rnd = new Random(seed);
    Instances dataSwapped = new Instances(data);
    IntStream.range(0, k)
        .forEach(i -> {
          int r = rnd.nextInt(dataSwapped.numInstances());
          Collections.swap(dataSwapped, i, r);
        });
    Instances result = new Instances(data, k);
    result.addAll(dataSwapped.stream()
        .limit(k)
        .distinct()
        .collect(Collectors.toList()));
    assert result.size() == k;
    return result;
  }


  /**
   * @param distances List streamOf vectors,
   *                  each vector represent the distances from this point to all centroids
   * @return maximum value streamOf the lowes distances streamOf vectors
   */
  public static double getMaxDist(List<double[]> distances) {
    return distances.stream()
        //.parallel() //TODO allow/test using parralel stream
        .mapToDouble(BUtils::minValue)
        .max()
        .getAsDouble();
  }

  /**
   * Keep only values below the threshold
   *
   * @param distances Array streamOf distances to centroids
   * @param threshold
   * @param keepMain  if true then at least keep the minimum value whatever the threshold is.
   * @return bitset that have all assignments set to 1 and rests are set to 0
   */
  public static BitSet bitSetOf(double[] distances, double threshold, boolean keepMain) {
    //get max distance index
    BitSet result = new BitSet(distances.length);
    for (int i = 0; i < distances.length; i++) {
      if (distances[i] <= threshold)
        result.set(i);
    }
    if (keepMain)
      result.set(minIndex(distances));
    return result;
  }


  /**
   * set values greater than thresholds to zero
   * //TODO (not used yet)
   *
   * @param a          :
   * @param threshold: threshold
   * @return : new array with lower values set to zero
   */
  public static double[] arrayThresholded(double[] a, double threshold) {
    double[] result = new double[a.length]; //defensive copy
    for (int i = 0; i < result.length; i++) {
      result[i] = a[i] < threshold ? a[i] : 0;
    }
    return result;
  }

  /**
   * @param distances
   * @param threshold
   * @return
   */
  public static List<double[]> distancesThresholded(List<double[]> distances,
                                                    double threshold) {
    //TODO (not used yet)
    return distances.stream()
        .parallel() //TODO check later
        .map(a -> arrayThresholded(a, threshold))
        .collect(Collectors.toList());
  }

  /**
   * Get the index streamOf minimum value in an array
   *
   * @param a
   * @return index streamOf minmum value
   */
  public static int minIndex(double[] a) {
    int minIndex = Integer.MAX_VALUE;
    double minValue = Double.MAX_VALUE;
    for (int i = 0; i < a.length; i++) {
      if (a[i] < minValue) {
        minValue = a[i];
        minIndex = i;
      }
    }
    return minIndex;
  }

  /**
   * Used to tranform java enumeration into java8 stream,
   * candidate usage in Instances methods :enumerateInstatnces, enumerateAttributes
   * benefit to do
   *
   * @param e   enumerateion
   * @param <T> class type
   * @return stream streamOf T type
   */
  public static <T> Stream<T> enum2Stream(Enumeration<T> e) {
    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(
            new Iterator<T>() {
              public T next() {
                return e.nextElement();
              }

              public boolean hasNext() {
                return e.hasMoreElements();
              }
            },
            Spliterator.ORDERED), false); //TODO: parallel flag to true later!
  }

  public static Stream<Instance> getStream(Instances data) {
    return data.stream();
  }

  public static Instances instancesOf(String fileName) {
    Instances result = null;
    try {
      return new Instances(new FileReader(fileName));
    } catch (IOException e) {
      log.log(Level.SEVERE, "Exception in loading {} file", fileName);
      e.printStackTrace();
    }
    return result;
  }

  public static double median(double[] values, double ratio) {
    double[] a = Arrays.copyOf(values, values.length);
    Arrays.sort(a);
    double part = a.length * ratio;
    int floor = (int) Math.floor(part);
    return part - floor < 0.00001 ?
        (a[floor] + a[floor - 1]) / 2 :
        a[floor];
  }

  public static int[] aboveMedianIndexes(double[] values, double ratio) {
    double median = median(values, ratio);
    System.out.println("median = " + median);
    return IntStream.range(0, values.length)
        .filter(i -> values[i] >= median)
        .toArray();
  }

  public static List<String> listOf(String rawOptions) {
    return Arrays.asList(rawOptions.split("\\s+"));
  }

  public static double maxValue(double... a) {
    double result = Double.MIN_VALUE;
    for (int i = 0; i < a.length; i++) {
      if (a[i] > result) result = a[i];
    }
    return result;
  }

  public static double minValue(double... a) {
    double result = Double.MAX_VALUE;
    for (int i = 0; i < a.length; i++) {
      if (a[i] < result) result = a[i];
    }
    return result;
  }

  public static double[] normalizeVector(double[] a) {
    //TODO check if sum > 0
    double sum = DoubleStream.of(a).sum();
    assert sum > 0;

    return DoubleStream.of(a)
        .map(i -> i / sum)
        .toArray();
  }


  public static List<int[]> transposeAndFilterZero(List<int[]> data) {
    //TODO check parameters
    final int numLabels = data.get(0).length;
    return IntStream.range(0, numLabels)
        .mapToObj(lIndex -> data.stream()
            .mapToInt(i -> i[lIndex])
            .filter(i -> i != 0)
            .toArray())
        .collect(Collectors.toList());
  }


  public static int combinProduct(int[] data) {
    return Combinations.of(2, data)
        .stream()
        .mapToInt(i -> i.get(0) * i.get(1))
        .sum();
  }

  public static int combinProduct(List<Integer> data) {
    return new Combinations<>(2, data)
        .stream()
        .mapToInt(i -> i.get(0) * i.get(1))
        .sum();
  }

  public static void main(String[] args) throws Exception {
    test2();
  }

  private static void test2() {
  }

  private static void test1() throws IOException {
    Instances data = new Instances(new FileReader("data/iris.arff"));
    data.setClassIndex(data.numAttributes() - 1);

    int[] labels = PairBasedEvaluation.countLabels(data, data.classAttribute().numValues());
    System.out.println("Arrays.toString(labels) = " + Arrays.toString(labels));

    int[] a1 = new int[]{1, 2, 0, 4};
    int[] a2 = new int[]{11, 22, 33, 44};
    List<int[]> counts = Arrays.asList(a1, a2);
    List<int[]> transpose = transposeAndFilterZero(counts);
    transpose.stream().forEach(i -> System.out.println(Arrays.toString(i)));
  }

  /**
   * Get distances streamOf one point to all clusterCentroids provided as parameter
   *
   * @param point
   * @param m_ClusterCentroids
   * @param df
   * @return
   */
  public static double[] distancesOnePoint(Instance point,
                                           Instances m_ClusterCentroids,
                                           DistanceFunction df) {
    return m_ClusterCentroids.stream()
        .mapToDouble(centroid -> df.distance(centroid, point))
        .toArray();
  }


  public static List<double[]> distances(Instances points,
                                         Instances m_ClusterCentroids,
                                         DistanceFunction df) {
    return points.stream()
        //.parallel() //TODO check performance here
        .map(point -> distancesOnePoint(point, m_ClusterCentroids, df))
        .collect(Collectors.toList());
  }

  public static BitSet bitSetofCluster(int c) {
    BitSet result = new BitSet(c);
    result.set(c);
    return result;
  }

  public static void fuseClusters(Instances data, Instances centroids,
                                  DistanceFunction df,
                                  double maxDistThreshold,
                                  double fusionThreshold) {
    List<double[]> distances = distances(data, centroids, df);
    Map<FInst, Double> collect = IntStream.range(0, distances.size())
        .boxed()
        .flatMap(line -> FInst.streamOf(line, distances.get(line), maxDistThreshold))
        .collect(Collectors.groupingBy(Function.identity(),
            Collectors.summingDouble(i -> i.sum)));

  }

  public static List<FInst> fuse(List<double[]> distances, double maxDistThreshold,
                                 double fusionThreshold) {
//    return IntStream.range(0, distances.size())
//        .boxed()
//        .flatMap(num -> FInst.streamOf(num, distances.get(num), maxDistThreshold))
//        .collect(Collectors.groupingBy(Function.identity(),
//            Collectors.summingDouble(i -> i.sum)));
    return null;
  }

}

class FInst {
  final public int inst;
  final public int cluster;
  final public double sum;

  FInst(int inst, int cluster, double sum) {
    this.inst = inst;
    this.cluster = cluster;
    this.sum = sum;
  }

  @Override
  public String toString() {
    return String.format("(inst: %d, cluster: %d, sum: %3.3f)", inst, cluster, sum);
  }

  public static FInst add(FInst i1, FInst i2) {
    return new FInst(i1.inst, i1.cluster, i1.sum + i2.sum);
  }

  public static Stream<FInst> streamOf(int line, double[] a, double maxDistThreshold) {
    return IntStream.range(0, a.length)
        .filter(cluster -> a[cluster] <= maxDistThreshold)
        .mapToObj(cluster -> new FInst(line, cluster, a[cluster]));
  }

  @Override
  public boolean equals(Object obj) {
    FInst that = (FInst) obj;
    return this.inst == that.inst
        && this.cluster == that.cluster;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.inst, this.cluster);
  }
}