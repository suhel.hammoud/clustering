package weka.clusterers;

import weka.classifiers.rules.DecisionTableHashKey;
import weka.core.*;
import weka.core.Capabilities.Capability;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

enum ALGORITHM {
  KMEAN, MCOKE, OMCOKE;

  public static SelectedTag selectedTag(String value) {
    return new SelectedTag(value, toTags());
  }

  public static Tag[] toTags() {
    ALGORITHM[] formulas = values();
    Tag[] result = new Tag[formulas.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = new Tag(i, formulas[i].name(), formulas[i].name());
    }
    return result;
  }
}

public class OmcokeCluster extends RandomizableClusterer implements
    NumberOfClustersRequestable, WeightedInstancesHandler,
    TechnicalInformationHandler {

  private static Logger log = Logger.getLogger(OmcokeCluster.class.getName());

  static {
    log.setLevel(Level.ALL);
  }

  //TODO
  NumberFormat nf = new DecimalFormat("00.00#");

  /**
   * for serialization.
   */
  static final long serialVersionUID = -3235809600124455376L;

  //* new Options

  protected String algorithm = ALGORITHM.OMCOKE.name(); //with meaures

  protected boolean useFusion = false; // pay attention to the algorithm for
  protected boolean useMeasures = true; // pay attention to the algorithm for
  protected boolean keepMain = false; //exclude the minimum distance from the threshold
  //* FT default = 1,range in [0-1] , 0: full fusion, 1: no fusion
  protected double fusionThreshold = 1.0;
  //* MXD maxdist variable, if 1 same as largest distance of an object that was assigned to a cluster.
  protected double maxDistThreshold = 1.0;

  volatile protected String okmStringResults = "";

  public SelectedTag getAlgorithm() {
    return ALGORITHM.selectedTag(algorithm);
  }

  public void setAlgorithm(SelectedTag tv) {
    this.algorithm = tv.getSelectedTag().toString();
  }

  public String algorithmTipText() {
    return "TODO: Algorithm tip text";
  }

  public boolean getUseFusion() {
    return useFusion;
  }

  public void setUseFusion(boolean useFusion) {
    this.useFusion = useFusion;
  }

  public String useFusionTipText() {
    return "TODO: use fusion tip text";
  }

  public boolean getUseMeasures() {
    return useMeasures;
  }

  public void setUseMeasures(boolean useMeasures) {
    this.useMeasures = useMeasures;
  }

  public String useMeasuresTipText() {
    return "TODO use measures tip text";
  }

  public double getFusionThreshold() {
    return fusionThreshold;
  }

  public void setFusionThreshold(double fusionThreshold) {
    this.fusionThreshold = fusionThreshold;
  }

  public String fusionThresholdTipText() {
    return "TODO fusion threshold tip text";
  }

  public double getMaxDistThreshold() {
    return maxDistThreshold;
  }

  public void setMaxDistThreshold(double maxDistThreshold) {
    this.maxDistThreshold = maxDistThreshold;
  }

  public String maxDistThresholdTipText() {
    return "TODO maxdist threshold tip text";
  }

  public void setKeepMain(boolean keepMain) {
    this.keepMain = keepMain;
  }

  public boolean getKeepMain() {
    return keepMain;
  }

  public String keepMainTipText() {
    return "TODO keepMain tip text";
  }


  /**
   * number of clusters to generate.
   */
  protected int m_NumClusters = 3;

  /**
   * Holds the initial start points, as supplied by the initialization method
   * used
   */
  protected Instances m_initialStartPoints;

  /**
   * holds the cluster centroids.
   */
  protected Instances m_ClusterCentroids;

  //* holds the cluster points
//  protected Instances[] m_ClusterPoints;

  //* holds the overlapped cluster points
//  Instances[] oClusterPoints = null;


  /**
   * Holds the standard deviations of the numeric attributes in each cluster.
   */
  protected Instances m_ClusterStdDevs;

  /**
   * For each cluster, holds the frequency counts for the values of each nominal
   * attribute.
   */
  protected double[][][] m_ClusterNominalCounts;
  protected double[][] m_ClusterMissingCounts;


  //TODO Suhel
  @Override
  public double[] distributionForInstance(Instance instance) throws Exception {
    return super.distributionForInstance(instance);
  }

  /**
   * Stats on the full data set for comparison purposes. In case the attribute
   * is numeric the value is the mean if is being used the Euclidian distance or
   * the median if Manhattan distance and if the attribute is nominal then it's
   * mode is saved.
   */
  protected double[] m_FullMeansOrMediansOrModes;
  protected double[] m_FullStdDevs;
  protected double[][] m_FullNominalCounts;
  protected double[] m_FullMissingCounts;

  /**
   * Display standard deviations for numeric atts.
   */
  protected boolean m_displayStdDevs;


  /**
   * The number of instances in each cluster.
   */
  protected double[] m_ClusterSizes;

  /**
   * Maximum number of iterations to be executed.
   */
  protected int m_MaxIterations = 10; //TODO change it back to 500

  /**
   * Keep track of the number of iterations completed before convergence.
   */
  protected int m_Iterations = 0;

  /**
   * Holds the squared errors for all clusters.
   */
  protected double[] m_squaredErrors;

  /**
   * the distance function used.
   */
  protected DistanceFunction m_DistanceFunction = new EuclideanDistance();


  /**
   * Assignments obtained.
   */
  protected int[] m_Assignments = null;


  /**
   * the default constructor.
   */
  public OmcokeCluster() {
    super();

    m_SeedDefault = 10;
    setSeed(m_SeedDefault);
  }


  @Override
  public TechnicalInformation getTechnicalInformation() {
    TechnicalInformation result;

    result = new TechnicalInformation(Type.INPROCEEDINGS);
    result.setValue(Field.AUTHOR, "Said");
    result.setValue(Field.TITLE, "OMCOKE");
    result.setValue(Field.BOOKTITLE, "");
    result.setValue(Field.YEAR, "2017");
    result.setValue(Field.PAGES, "***-***");

    return result;
  }

  /**
   * Returns a string describing this clusterer.
   *
   * @return a description of the evaluator suitable for displaying in the
   * explorer/experimenter gui
   */
  public String globalInfo() {
    return "Cluster data using the OMCOKE algorithm. Can use either "
        + "the Euclidean distance (default) or the Manhattan distance."
        + " If the Manhattan distance is used, then centroids are computed "
        + "as the component-wise median rather than mean."
        + " For more information see:\n\n" + getTechnicalInformation().toString();
  }

  /**
   * Returns default capabilities of the clusterer.
   *
   * @return the capabilities of this clusterer
   */
  @Override
  public Capabilities getCapabilities() {
    Capabilities result = super.getCapabilities();
    result.disableAll();
    result.enable(Capability.NO_CLASS);

    // attributes
    result.enable(Capability.NOMINAL_ATTRIBUTES);
    result.enable(Capability.NUMERIC_ATTRIBUTES);
    result.enable(Capability.MISSING_VALUES);

    return result;
  }


  /**
   * Generates a clusterer. Has to initialize all fields of the clusterer that
   * are not being set via options.
   *
   * @param instances set of instances serving as training data
   * @throws Exception if the clusterer has not been generated successfully
   */
  @Override
  public void buildClusterer(Instances instances) throws Exception {
    Instances data = new Instances(instances);
    okmStringResults = "";

    InitClusters init = new InitClusters().invoke(data);

    Instances[] m_ClusterPoints = null;

    switch (ALGORITHM.valueOf(algorithm)) {
      case KMEAN:
        m_ClusterPoints = buildClustererKMean(init.instances, init.clusterAssignments);
        break;
      case MCOKE:
        m_ClusterPoints = buildClustererMCOKE(data);
        break;
      case OMCOKE:
        m_ClusterPoints = buildClustererOMCOKE(data);
        break;
    }

    calcStats(data, m_ClusterPoints);
    log.info("\n" + m_ClusterCentroids.stream()
        .map(i -> i.toString())
        .collect(Collectors.joining("\n")));
  }


  private Instances[] buildClustererMCOKE(Instances instances) {
    //TODO implementation
    return null;
  }


  /**
   * Map all assignments to its clusters, allow overlapping
   *
   * @param assignments
   * @return Map of entries : e.key = centroidIndex, e.value = List of indexes of instances mapped to this cluster:
   */
  public static Map<Integer, List<Integer>> toClusters(List<BitSet> assignments) {
    //TODO change whole implementation to functional style using java 8 streams

    HashMap<Integer, List<Integer>> result = new HashMap<>(6);//TODO set better initialCapacity
    for (int line = 0; line < assignments.size(); line++) {
      final int ln = line;
      final BitSet bs = assignments.get(line);
      bs.stream()
          .forEach(clusterIndex -> {
            List<Integer> list = result.get(clusterIndex);
            if (list == null) {
              list = new ArrayList<>();
              result.put(clusterIndex, list);
            }
            list.add(ln);
          });
    }
    return result;
  }


  public static Instance moveCentroid(Instances data,
                                      List<Integer> points) {
    return new DenseInstance(1.0,
        moveCentroidVals(data, points));
  }

  public static double[] moveCentroidVals(Instances data,
                                          List<Integer> points) {
    //use only numerical attributes,
    // TODO handle nominal values
    // TODO hanld missing values
    double[] vals = new double[data.numAttributes()];

    double[][] nominalDists = new double[data.numAttributes()][];
    double[] weightMissing = new double[data.numAttributes()];
    double[] weightNonMissing = new double[data.numAttributes()];

    // Quickly calculate some relevant statistics
    for (int j = 0; j < data.numAttributes(); j++) {
      if (data.attribute(j).isNominal()) {
        nominalDists[j] = new double[data.attribute(j).numValues()];
      }
    }
    for (int instIndex : points) {
      Instance inst = data.instance(instIndex);
//    for (Instance inst : members) {
      for (int j = 0; j < data.numAttributes(); j++) {
        if (inst.isMissing(j)) {
          weightMissing[j] += inst.weight();
        } else {
          weightNonMissing[j] += inst.weight();
          if (data.attribute(j).isNumeric()) {
            vals[j] += inst.weight() * inst.value(j); // Will be overwritten in Manhattan case
          } else {
            nominalDists[j][(int) inst.value(j)] += inst.weight();
          }
        }
      }
    }
    for (int j = 0; j < data.numAttributes(); j++) {
      if (data.attribute(j).isNumeric()) {
        if (weightNonMissing[j] > 0) {
          vals[j] /= weightNonMissing[j];
        } else {
          vals[j] = Utils.missingValue();
        }
      } else {
        double max = -Double.MAX_VALUE;
        double maxIndex = -1;
        for (int i = 0; i < nominalDists[j].length; i++) {
          if (nominalDists[j][i] > max) {
            max = nominalDists[j][i];
            maxIndex = i;
          }
          if (max < weightMissing[j]) {
            vals[j] = Utils.missingValue();
          } else {
            vals[j] = maxIndex;
          }
        }
      }
    }
    return vals;
  }


  public static Instances calculateCentroids(Instances data, List<BitSet> assignments,
                                             int numClusters, DistanceFunction df) {

    Instances result = new Instances(data, numClusters);
    Map<Integer, List<Integer>> mappedAssignments = toClusters(assignments);
//    assert numClusters == clusteredAssignments.size(); //todo check later
    if (numClusters != mappedAssignments.size()) {
      log.warning(String.format("numClusters = %d, mappedAssignments = %d",
          numClusters, mappedAssignments.size()));
    }
    result.addAll(
        IntStream.range(0, numClusters)
            .filter(i -> mappedAssignments.get(i) != null)
            .mapToObj(i -> moveCentroid(data, mappedAssignments.get(i)))
            .collect(Collectors.toList()));

    return result;
  }


  /**
   * @param data
   * @throws Exception
   */
  private Instances[] buildClustererOMCOKE(Instances data) {

    Instances[] m_ClusterPoints = null;

    boolean converged = false;

    /* overlapped cluster points */
    List<Integer> outlier = null;
    int[] bestAssignments = null;


    List<BitSet> assignments = new ArrayList<>();

    //TODO make sure not to pick up similar points
    final Instances centroids = BUtils.sample(data, m_NumClusters, m_Seed);

    int iteration = 0;

    while (!converged) {

      iteration++;

      // distances from each point to centroids
      List<double[]> distances = BUtils.distances(data, centroids, m_DistanceFunction);

//      bestAssignments = distances.stream()
//          .mapToInt(i -> BUtils.minIndex(i))
//          .toArray();
//      int distinct = (int) Arrays.stream(bestAssignments)
//          .distinct()
//          .count();
//      log.info(String.format("distinct best assignments: %d", distinct));


      // maximum of "lowest" distances among all points
      double maxDist = BUtils.getMaxDist(distances);

      //threshold to cut off the outliers as a ratio of maxDist value
      final double distanceThreshold = maxDist * maxDistThreshold;

      //find overlapping assignments and apply threshold at the same time
      List<BitSet> tempAssignments = distances
          .stream()
          .map(d -> BUtils.bitSetOf(d, distanceThreshold, keepMain))
          .collect(Collectors.toList());

      //converged is false for any changes from last assignments
      converged = assignments.size() == tempAssignments.size()
          && !IntStream.range(0, tempAssignments.size())
          .filter(a -> !assignments.get(a).equals(tempAssignments.get(a)))
          .findAny()
          .isPresent();

      log.info(String.format("iteration = %d, converged = %s", iteration, converged));

      //outlier consists of all non assigned points,
      // (if keepMain then no outlier should be found)
      outlier = IntStream.range(0, tempAssignments.size())
          .filter(index -> tempAssignments.get(index).isEmpty())
          .boxed()
          .collect(Collectors.toList());

      log.info(String.format("outlier size = %d", outlier.size()));

      // update centroids
      centroids.clear();

      centroids.addAll(
          calculateCentroids(data,
              tempAssignments,
              m_NumClusters,
              m_DistanceFunction));
      centroids.setClassIndex(centroids.numAttributes() - 1);
      m_ClusterCentroids = centroids;//TODO delete later

      log.info(String.format("Number of centroids = %d", centroids.numInstances()));

//      Print new centroids
//      log.info(String.format("centroids: \n%s", centroids
//          .stream()
//          .map(i -> i.toString())
//          .collect(Collectors.joining("\n"))
//      ));


      if (iteration == m_MaxIterations) {
        converged = true;
      }

      // check empty clusters
      int emptyClusterCount = m_NumClusters - centroids.size();
      log.info(String.format("emptyClusterCount = %d", emptyClusterCount));

      // TODO add update stats code here

      if (!converged) {
        m_ClusterNominalCounts = new double[m_NumClusters][data.numAttributes()][0];
        assignments.clear();
        assignments.addAll(tempAssignments);
      } else {
        /** converged */
        m_ClusterCentroids = centroids;

        // save memory!
        m_DistanceFunction.clean();

        //* setup global variable
        m_Iterations = iteration;

//        //update map of cluster points
//        final Instances[] oClusterPoints = new Instances[centroids.size()];
//        toClusters(tempAssignments).entrySet().stream()
//            .forEach(entry -> {
//              Instances instances = new Instances(data, 0);
//              instances.setClassIndex(data.classIndex());
//
//              instances.addAll(entry.getValue().stream()
//                  .map(line -> data.instance(line))
//                  .collect(Collectors.toList()));
//              oClusterPoints[entry.getKey()] = instances;
//            });

        m_ClusterPoints = toClustersInInstances(data, outlier, centroids);


        int allPoints = Arrays.stream(m_ClusterPoints)
            .mapToInt(i -> i.numInstances())
            .sum();

        if (useMeasures) {
          StringJoiner sj = new StringJoiner("\n");
          sj.add(String.format("\nNumber of centroids = %d centroids", m_ClusterCentroids.numInstances()));
          sj.add(String.format("\nAll Points = %d points", allPoints));
          sj.add(String.format("Number of Outliers = %d points", outlier.size()));
          sj.add(String.format("Outlier indexes = %s", outlier.toString()));

          Map<Integer, List<Integer>> tmpMap = toClusters(tempAssignments);
          PairBasedEvaluation eval = new PairBasedEvaluation(data);
          eval.calc(tmpMap);
          sj.add(eval.getResults());

          sj.add("oooooooooooooooooooooooooooooooo");
          OPairBasedEvaluation eval2 = OPairBasedEvaluation.of(data);
                    eval2.calc(m_ClusterPoints, allPoints);
                    sj.add(eval.getResults());

//          OPairBasedEvaluation oeval = OPairBasedEvaluation.of(data);
//          oeval.calc(tmpMap);
//          sj.add("\n+++++++++++++++++++++++++++++\nOverlapped Pair Based Evaluation");
//          sj.add(oeval.getResults());
          okmStringResults += sj.toString();
          log.info(" =========== measure ============\n" + eval.getResults());
        }
      }
    }
    return m_ClusterPoints;
  }


  //
  private Instances[] buildClustererKMean(Instances instances, int[] clusterAssignments) {
    int i;
    boolean converged = false;
    int emptyClusterCount;
    Instances[] tempIClusters = new Instances[m_NumClusters];


    int iteration = 0;

    while (!converged) {


      emptyClusterCount = 0;
      iteration++;
      converged = true;

      for (i = 0; i < instances.numInstances(); i++) {
        Instance toCluster = instances.instance(i);
        int newC =
            clusterProcessedInstance(
                toCluster,
                false);
        if (newC != clusterAssignments[i]) {
          converged = false;
        }
        clusterAssignments[i] = newC;
      }

      // update centroids
      m_ClusterCentroids = new Instances(instances, m_NumClusters);
      for (i = 0; i < m_NumClusters; i++) {
        tempIClusters[i] = new Instances(instances, 0);
      }
      for (i = 0; i < instances.numInstances(); i++) {
        tempIClusters[clusterAssignments[i]].add(instances.instance(i));
      }
      for (i = 0; i < m_NumClusters; i++) {
        if (tempIClusters[i].numInstances() == 0) {
          // empty cluster
          emptyClusterCount++;
        } else {
          moveCentroid(i, tempIClusters[i], true, true);
        }
      }

      if (iteration == m_MaxIterations) {
        converged = true;
      }

      if (emptyClusterCount > 0) {
        //* reduce tempIClusters to t ones
        m_NumClusters -= emptyClusterCount;
        if (converged) {
          Instances[] t = new Instances[m_NumClusters];
          int index = 0;
          for (int k = 0; k < tempIClusters.length; k++) {
            if (tempIClusters[k].numInstances() > 0) {
              t[index] = tempIClusters[k];

              for (i = 0; i < tempIClusters[k].numAttributes(); i++) {
                m_ClusterNominalCounts[index][i] = m_ClusterNominalCounts[k][i];
              }
              index++;
            }
          }
          tempIClusters = t;
        } else {
          tempIClusters = new Instances[m_NumClusters];
        }
      }

      if (!converged) {
        m_ClusterNominalCounts = new double[m_NumClusters][instances.numAttributes()][0];
      }
    }

    // save memory!
    m_DistanceFunction.clean();

    //* setup global variable
    m_Iterations = iteration;
    return tempIClusters;
  }

  private void calcStats(Instances data, Instances[] clusterPoints) {
    //TODO may use parallel execution to speedup the calculations
    m_squaredErrors = new double[m_NumClusters];
    m_ClusterNominalCounts = new double[m_NumClusters][data.numAttributes()][0];
    m_ClusterMissingCounts = new double[m_NumClusters][data.numAttributes()];

    int i;// calculate errors
    for (i = 0; i < data.numInstances(); i++) {
      clusterProcessedInstance(data.instance(i), true);
    }

    if (m_displayStdDevs) {
      m_ClusterStdDevs = new Instances(data, m_NumClusters);
    }
    m_ClusterSizes = new double[m_NumClusters];
    for (i = 0; i < m_NumClusters; i++) {
      if (m_displayStdDevs) {
        double[] vals2 = clusterPoints[i].variances();
        for (int j = 0; j < data.numAttributes(); j++) {
          if (data.attribute(j).isNumeric()) {
            vals2[j] = Math.sqrt(vals2[j]);
          } else {
            vals2[j] = Utils.missingValue();
          }
        }
        m_ClusterStdDevs.add(new DenseInstance(1.0, vals2));
      }
      m_ClusterSizes[i] = clusterPoints[i].sumOfWeights();
    }
  }


  /**
   * Move the centroid to it's new coordinates. Generate the centroid
   * coordinates based on it's members (objects assigned to the cluster of the
   * centroid) and the distance function being used.
   *
   * @param centroidIndex          index of the centroid which the coordinates will be
   *                               computed
   * @param members                the objects that are assigned to the cluster of this
   *                               centroid
   * @param updateClusterInfo      if the method is supposed to update the m_Cluster
   *                               arrays
   * @param addToCentroidInstances true if the method is to add the computed
   *                               coordinates to the Instances holding the centroids
   * @return the centroid coordinates
   */
  protected double[] moveCentroid(int centroidIndex, Instances members,
                                  boolean updateClusterInfo, boolean addToCentroidInstances) {

    double[] vals = new double[members.numAttributes()];
    double[][] nominalDists = new double[members.numAttributes()][];
    double[] weightMissing = new double[members.numAttributes()];
    double[] weightNonMissing = new double[members.numAttributes()];

    // Quickly calculate some relevant statistics
    for (int j = 0; j < members.numAttributes(); j++) {
      if (members.attribute(j).isNominal()) {
        nominalDists[j] = new double[members.attribute(j).numValues()];
      }
    }
    for (Instance inst : members) {
      for (int j = 0; j < members.numAttributes(); j++) {
        if (inst.isMissing(j)) {
          weightMissing[j] += inst.weight();
        } else {
          weightNonMissing[j] += inst.weight();
          if (members.attribute(j).isNumeric()) {
            vals[j] += inst.weight() * inst.value(j); // Will be overwritten in Manhattan case
          } else {
            nominalDists[j][(int) inst.value(j)] += inst.weight();
          }
        }
      }
    }
    for (int j = 0; j < members.numAttributes(); j++) {
      if (members.attribute(j).isNumeric()) {
        if (weightNonMissing[j] > 0) {
          vals[j] /= weightNonMissing[j];
        } else {
          vals[j] = Utils.missingValue();
        }
      } else {
        double max = -Double.MAX_VALUE;
        double maxIndex = -1;
        for (int i = 0; i < nominalDists[j].length; i++) {
          if (nominalDists[j][i] > max) {
            max = nominalDists[j][i];
            maxIndex = i;
          }
          if (max < weightMissing[j]) {
            vals[j] = Utils.missingValue();
          } else {
            vals[j] = maxIndex;
          }
        }
      }
    }

    //* TODO not used for now, review and decide how to change later
    if (m_DistanceFunction instanceof ManhattanDistance) {

      // Need to replace means by medians
      Instances sortedMembers = null;
      int middle = (members.numInstances() - 1) / 2;
      boolean dataIsEven = ((members.numInstances() % 2) == 0);
      sortedMembers = new Instances(members); //TODO use sortedMemebers = members for performance
      for (int j = 0; j < members.numAttributes(); j++) {
        if ((weightNonMissing[j] > 0) && members.attribute(j).isNumeric()) {
          // singleton special case
          if (members.numInstances() == 1) {
            vals[j] = members.instance(0).value(j);
          } else {
            vals[j] = sortedMembers.kthSmallestValue(j, middle + 1);
            if (dataIsEven) {
              vals[j] = (vals[j] + sortedMembers.kthSmallestValue(j, middle + 2)) / 2;
            }
          }
        }
      }
    }

    if (updateClusterInfo) {
      for (int j = 0; j < members.numAttributes(); j++) {
        m_ClusterMissingCounts[centroidIndex][j] = weightMissing[j];
        m_ClusterNominalCounts[centroidIndex][j] = nominalDists[j];
      }
    }

    if (addToCentroidInstances) {
      m_ClusterCentroids.add(new DenseInstance(1.0, vals));
    }

    return vals;
  }


  /**
   * clusters an instance that has been through the filters.
   *
   * @param instance     the instance to assign a cluster to
   * @param updateErrors if true, update the within clusters sum of errors
   * @return a cluster number
   */
  private int clusterProcessedInstance(Instance instance, boolean updateErrors) {
    double minDist = Integer.MAX_VALUE;
    int bestCluster = 0;
    for (int i = 0; i < m_NumClusters; i++) {
      double dist;
      dist =
          m_DistanceFunction.distance(instance, m_ClusterCentroids.instance(i));
      if (dist < minDist) {
        minDist = dist;
        bestCluster = i;
      }
    }
    if (updateErrors) {
      if (m_DistanceFunction instanceof EuclideanDistance) {
        // Euclidean distance to Squared Euclidean distance
        minDist *= minDist * instance.weight();
      }
      m_squaredErrors[bestCluster] += minDist;
    }
    return bestCluster;
  }

  /**
   * Classifies a given instance.
   *
   * @param instance the instance to be assigned to a cluster
   * @return the number of the assigned cluster as an interger if the class is
   * enumerated, otherwise the predicted value
   * @throws Exception if instance could not be classified successfully
   */
  @Override
  public int clusterInstance(Instance instance) {

//    double[] doubles = BUtils.distancesOnePoint(instance, m_ClusterCentroids, m_DistanceFunction);
//    log.info(String.format("CI: %s -> %s // %d",
//        instance.toString(),
//        BUtils.toStringA(doubles),
//        BUtils.minIndex(doubles)));
//    return BUtils.minIndex(doubles);
    return clusterProcessedInstance(instance, false);
//    int tmpValue = clusterOneInstance(instance,
//        m_ClusterCentroids, m_DistanceFunction);
//    return tmpValue;
  }


  public static int clusterOneInstance(Instance point,
                                       Instances centroids,
                                       DistanceFunction df) {
    double[] distances = BUtils.distancesOnePoint(point, centroids, df);
    return BUtils.minIndex(distances);
  }

  private Instances[] toClustersInInstances(Instances data,
                                            List<Integer> outliers,
                                            Instances centroids) {
    Instances[] result = new Instances[centroids.size()];
    for (int i = 0; i < result.length; i++) {
      result[i] = new Instances(data, data.numInstances());
    }

    IntStream.range(0, data.numInstances())
        .filter(i -> !outliers.contains(i))
        .mapToObj(i -> data.instance(i))
        .peek(j -> j = j)
        .forEach(point -> {
          int c = clusterOneInstance(point, centroids, m_DistanceFunction);
          result[c].add(point);
        });
    return result;
  }

  /**
   * Returns the number of clusters.
   *
   * @return the number of clusters generated for a training dataset.
   * @throws Exception if number of clusters could not be returned successfully
   */
  @Override
  public int numberOfClusters() throws Exception {
    return m_NumClusters;
  }

  /**
   * Returns an enumeration describing the available options.
   *
   * @return an enumeration of all the available options.
   */
  @Override
  public Enumeration<Option> listOptions() {
    Vector<Option> result = new Vector<Option>();
//
    result.addElement(new Option("\tAlgorithm to be used.\n" + "\t(default KMEAN).",
        "algorithm", 1, "-algorithm <kmean, MCOKE, OMCOKE>"));

    result.addElement(new Option("\tUse fusion in algorithm.\n" + "\t(default false).",
        "fusion", 0, "-fusion"));

    result.addElement(new Option("\tCalc measures for algorithm.\n" + "\t(default false).",
        "measures", 0, "-measures"));

    result.addElement(new Option("\tExclude minimum distance class from thresholding.\n" + "\t(default false).",
        "KM", 0, "-KM"));

    result.addElement(new Option("\tFustion threshold.\n" + "\t(default 1, no fusion).",
        "FT", 1, "-FT <num>"));

    result.addElement(new Option("\tMaxDist threshold.\n" + "\t(default 1).",
        "MXD", 1, "-MXD <num>"));

    result.addElement(new Option("\tNumber of clusters.\n" + "\t(default 2).",
        "N", 1, "-N <num>"));


    result.addElement(new Option("\tDisplay std. deviations for centroids.\n",
        "V", 0, "-V"));
    result.addElement(new Option(
        "\tDon't replace missing values with mean/mode.\n", "M", 0, "-M"));

    result.add(new Option("\tDistance function to use.\n"
        + "\t(default: weka.core.EuclideanDistance)", "A", 1,
        "-A <classname and options>"));

    result.add(new Option("\tMaximum number of iterations.\n", "I", 1,
        "-I <num>"));


    result.addAll(Collections.list(super.listOptions()));

    return result.elements();
  }

  /**
   * Returns the tip text for this property.
   *
   * @return tip text for this property suitable for displaying in the
   * explorer/experimenter gui
   */
  public String numClustersTipText() {
    return "set number of clusters";
  }

  /**
   * set the number of clusters to generate.
   *
   * @param n the number of clusters to generate
   * @throws Exception if number of clusters is negative
   */
  @Override
  public void setNumClusters(int n) throws Exception {
    if (n <= 0) {
      throw new Exception("Number of clusters must be > 0");
    }
    m_NumClusters = n;
  }

  /**
   * gets the number of clusters to generate.
   *
   * @return the number of clusters to generate
   */
  public int getNumClusters() {
    return m_NumClusters;
  }


  /**
   * Returns the tip text for this property.
   *
   * @return tip text for this property suitable for displaying in the
   * explorer/experimenter gui
   */
  public String maxIterationsTipText() {
    return "set maximum number of iterations";
  }

  /**
   * set the maximum number of iterations to be executed.
   *
   * @param n the maximum number of iterations
   * @throws Exception if maximum number of iteration is smaller than 1
   */
  public void setMaxIterations(int n) throws Exception {
    if (n <= 0) {
      throw new Exception("Maximum number of iterations must be > 0");
    }
    m_MaxIterations = n;
  }

  /**
   * gets the number of maximum iterations to be executed.
   *
   * @return the number of clusters to generate
   */
  public int getMaxIterations() {
    return m_MaxIterations;
  }

  /**
   * Returns the tip text for this property.
   *
   * @return tip text for this property suitable for displaying in the
   * explorer/experimenter gui
   */
  public String displayStdDevsTipText() {
    return "Display std deviations of numeric attributes "
        + "and counts of nominal attributes.";
  }

  /**
   * Sets whether standard deviations and nominal count. Should be displayed in
   * the clustering output.
   *
   * @param stdD true if std. devs and counts should be displayed
   */
  public void setDisplayStdDevs(boolean stdD) {
    m_displayStdDevs = stdD;
  }

  /**
   * Gets whether standard deviations and nominal count. Should be displayed in
   * the clustering output.
   *
   * @return true if std. devs and counts should be displayed
   */
  public boolean getDisplayStdDevs() {
    return m_displayStdDevs;
  }

  /**
   * Returns the tip text for this property.
   *
   * @return tip text for this property suitable for displaying in the
   * explorer/experimenter gui
   */
  public String dontReplaceMissingValuesTipText() {
    return "Replace missing values globally with mean/mode.";
  }


  /**
   * Returns the tip text for this property.
   *
   * @return tip text for this property suitable for displaying in the
   * explorer/experimenter gui
   */
  public String distanceFunctionTipText() {
    return "The distance function to use for instances comparison "
        + "(default: weka.core.EuclideanDistance). ";
  }

  /**
   * returns the distance function currently in use.
   *
   * @return the distance function
   */
  public DistanceFunction getDistanceFunction() {
    return m_DistanceFunction;
  }

  /**
   * sets the distance function to use for instance comparison.
   *
   * @param df the new distance function to use
   * @throws Exception if instances cannot be processed
   */
  public void setDistanceFunction(DistanceFunction df) throws Exception {
    if (!(df instanceof EuclideanDistance)
        && !(df instanceof ManhattanDistance)) {
      throw new Exception(
          "SimpleKMeans currently only supports the Euclidean and Manhattan distances.");
    }
    m_DistanceFunction = df;
  }

  /**
   * Returns the tip text for this property.
   *
   * @return tip text for this property suitable for displaying in the
   * explorer/experimenter gui
   */
  public String preserveInstancesOrderTipText() {
    return "Preserve order of instances.";
  }


  /**
   * @param options the list of options as an array of strings
   * @throws Exception if an option is not supported
   */
  @Override
  public void setOptions(String[] options) throws Exception {

    algorithm = Utils.getOption("algorithm", options);
    useFusion = Boolean.parseBoolean(Utils.getOption("fusion", options));
    useMeasures = Boolean.parseBoolean(Utils.getOption("measures", options));
    keepMain = Boolean.parseBoolean(Utils.getOption("KM", options));
    fusionThreshold = Double.parseDouble(Utils.getOption("FT", options));
    maxDistThreshold = Double.parseDouble(Utils.getOption("MXD", options));


    m_displayStdDevs = Utils.getFlag("V", options);


    String optionString = Utils.getOption('N', options);

    if (optionString.length() != 0) {
      setNumClusters(Integer.parseInt(optionString));
    }

    optionString = Utils.getOption("I", options);
    if (optionString.length() != 0) {
      setMaxIterations(Integer.parseInt(optionString));
    }

    String distFunctionClass = Utils.getOption('A', options);
    if (distFunctionClass.length() != 0) {
      String distFunctionClassSpec[] = Utils.splitOptions(distFunctionClass);
      if (distFunctionClassSpec.length == 0) {
        throw new Exception("Invalid DistanceFunction specification string.");
      }
      String className = distFunctionClassSpec[0];
      distFunctionClassSpec[0] = "";

      setDistanceFunction((DistanceFunction) Utils.forName(
          DistanceFunction.class, className, distFunctionClassSpec));
    } else {
      setDistanceFunction(new EuclideanDistance());
    }

    super.setOptions(options);

    Utils.checkForRemainingOptions(options);
  }


  /**
   * Gets the current settings of SimpleKMeans.
   *
   * @return an array of strings suitable for passing to setOptions()
   */
  @Override
  public String[] getOptions() {

    Vector<String> result = new Vector<String>();

    result.add("-algorithm");
    result.add("" + algorithm);

    result.add("-fusion");
    result.add("" + useFusion);

    result.add("-measures");
    result.add("" + useMeasures);

    result.add("-KM");
    result.add("" + keepMain);

    result.add("-FT");
    result.add("" + fusionThreshold);

    result.add("-MXD");
    result.add("" + maxDistThreshold);

    if (m_displayStdDevs) {
      result.add("-V");
    }


    result.add("-N");
    result.add("" + getNumClusters());

    result.add("-A");
    result.add((m_DistanceFunction.getClass().getName() + " " + Utils
        .joinOptions(m_DistanceFunction.getOptions())).trim());

    result.add("-I");
    result.add("" + getMaxIterations());


    Collections.addAll(result, super.getOptions());

    return result.toArray(new String[result.size()]);
  }

  /**
   * return a string describing this clusterer.
   *
   * @return a description of the clusterer as a string
   */
  @Override
  public String toString() {
    if (m_ClusterCentroids == null) {
      return "No clusterer built yet!";
    }

    int maxWidth = 0;
    int maxAttWidth = 0;
    boolean containsNumeric = false;
    for (int i = 0; i < m_NumClusters; i++) {
      for (int j = 0; j < m_ClusterCentroids.numAttributes(); j++) {
        if (m_ClusterCentroids.attribute(j).name().length() > maxAttWidth) {
          maxAttWidth = m_ClusterCentroids.attribute(j).name().length();
        }
        if (m_ClusterCentroids.attribute(j).isNumeric()) {
          containsNumeric = true;
          double width =
              Math.log(Math.abs(m_ClusterCentroids.instance(i).value(j)))
                  / Math.log(10.0);

          if (width < 0) {
            width = 1;
          }
          // decimal + # decimal places + 1
          width += 6.0;
          if ((int) width > maxWidth) {
            maxWidth = (int) width;
          }
        }
      }
    }

    for (int i = 0; i < m_ClusterCentroids.numAttributes(); i++) {
      if (m_ClusterCentroids.attribute(i).isNominal()) {
        Attribute a = m_ClusterCentroids.attribute(i);
        for (int j = 0; j < m_ClusterCentroids.numInstances(); j++) {
          String val = a.value((int) m_ClusterCentroids.instance(j).value(i));
          if (val.length() > maxWidth) {
            maxWidth = val.length();
          }
        }
        for (int j = 0; j < a.numValues(); j++) {
          String val = a.value(j) + " ";
          if (val.length() > maxAttWidth) {
            maxAttWidth = val.length();
          }
        }
      }
    }

    if (m_displayStdDevs) {
      // check for maximum width of maximum frequency count
      for (int i = 0; i < m_ClusterCentroids.numAttributes(); i++) {
        if (m_ClusterCentroids.attribute(i).isNominal()) {
          int maxV = Utils.maxIndex(m_FullNominalCounts[i]);
          /*
           * int percent = (int)((double)m_FullNominalCounts[i][maxV] /
           * Utils.sum(m_ClusterSizes) * 100.0);
           */
          int percent = 6; // max percent width (100%)
          String nomV = "" + m_FullNominalCounts[i][maxV];
          // + " (" + percent + "%)";
          if (nomV.length() + percent > maxWidth) {
            maxWidth = nomV.length() + 1;
          }
        }
      }
    }

    // check for size of cluster sizes
    for (double m_ClusterSize : m_ClusterSizes) {
      String size = "(" + m_ClusterSize + ")";
      if (size.length() > maxWidth) {
        maxWidth = size.length();
      }
    }

    if (m_displayStdDevs && maxAttWidth < "missing".length()) {
      maxAttWidth = "missing".length();
    }

    String plusMinus = "+/-";
    maxAttWidth += 2;
    if (m_displayStdDevs && containsNumeric) {
      maxWidth += plusMinus.length();
    }
    if (maxAttWidth < "Attribute".length() + 2) {
      maxAttWidth = "Attribute".length() + 2;
    }

    if (maxWidth < "Full Data".length()) {
      maxWidth = "Full Data".length() + 1;
    }

    if (maxWidth < "missing".length()) {
      maxWidth = "missing".length() + 1;
    }

    StringBuffer temp = new StringBuffer();
    temp.append(String.format("\n %s \n======\n", algorithm));
    temp.append("\nNumber of iterations: " + m_Iterations);

    temp.append("\n");
    if (m_DistanceFunction instanceof EuclideanDistance) {
      temp.append("Within cluster sum of squared errors: "
          + Utils.sum(m_squaredErrors));
    } else {
      temp.append("Sum of within cluster distances: "
          + Utils.sum(m_squaredErrors));
    }

    temp.append("\n\nInitial starting points ( random):\n");

    temp.append("\n");
    for (int i = 0; i < m_initialStartPoints.numInstances(); i++) {
      temp.append("Cluster " + i + ": " + m_initialStartPoints.instance(i))
          .append("\n");
    }

    //temp.append("\nMissing values globally replaced with mean/mode");

    temp.append("\n\nFinal cluster centroids:\n");
    temp.append(pad("Cluster#", " ", (maxAttWidth + (maxWidth * 2 + 2))
        - "Cluster#".length(), true));

    temp.append("\n");
    temp.append(pad("Attribute", " ", maxAttWidth - "Attribute".length(), false));

    temp.append(pad("Full Data", " ", maxWidth + 1 - "Full Data".length(), true));

    // cluster numbers
    for (int i = 0; i < m_NumClusters; i++) {
      String clustNum = "" + i;
      temp.append(pad(clustNum, " ",
          maxWidth + 1 - clustNum.length(),
          true));
    }
    temp.append("\n");

    // cluster sizes
    String cSize = "(" + Utils.sum(m_ClusterSizes) + ")";
    temp.append(pad(cSize, " ", maxAttWidth + maxWidth + 1 - cSize.length(),
        true));
    for (int i = 0; i < m_NumClusters; i++) {
      cSize = "(" + m_ClusterSizes[i] + ")";
      temp.append(pad(cSize, " ", maxWidth + 1 - cSize.length(), true));
    }
    temp.append("\n");

    temp.append(pad("", "=",
        maxAttWidth
            + (maxWidth * (m_ClusterCentroids.numInstances() + 1)
            + m_ClusterCentroids.numInstances() + 1), true));
    temp.append("\n");

    for (int i = 0; i < m_ClusterCentroids.numAttributes(); i++) {
      String attName = m_ClusterCentroids.attribute(i).name();
      temp.append(attName);
      for (int j = 0; j < maxAttWidth - attName.length(); j++) {
        temp.append(" ");
      }

      String strVal;
      String valMeanMode;
      // full data
      if (m_ClusterCentroids.attribute(i).isNominal()) {
        if (m_FullMeansOrMediansOrModes[i] == -1) { // missing
          valMeanMode =
              pad("missing", " ", maxWidth + 1 - "missing".length(), true);
        } else {
          valMeanMode =
              pad(
                  (strVal =
                      m_ClusterCentroids.attribute(i).value(
                          (int) m_FullMeansOrMediansOrModes[i])), " ", maxWidth + 1
                      - strVal.length(), true);
        }
      } else {
        if (Double.isNaN(m_FullMeansOrMediansOrModes[i])) {
          valMeanMode =
              pad("missing", " ", maxWidth + 1 - "missing".length(), true);
        } else {
          valMeanMode =
              pad(
                  (strVal =
                      Utils.doubleToString(m_FullMeansOrMediansOrModes[i], maxWidth,
                          4).trim()), " ", maxWidth + 1 - strVal.length(), true);
        }
      }
      temp.append(valMeanMode);

      for (int j = 0; j < m_NumClusters; j++) {
        if (m_ClusterCentroids.attribute(i).isNominal()) {
          if (m_ClusterCentroids.instance(j).isMissing(i)) {
            valMeanMode =
                pad("missing", " ", maxWidth + 1 - "missing".length(), true);
          } else {
            valMeanMode =
                pad(
                    (strVal =
                        m_ClusterCentroids.attribute(i).value(
                            (int) m_ClusterCentroids.instance(j).value(i))), " ",
                    maxWidth + 1 - strVal.length(), true);
          }
        } else {
          if (m_ClusterCentroids.instance(j).isMissing(i)) {
            valMeanMode =
                pad("missing", " ", maxWidth + 1 - "missing".length(), true);
          } else {
            valMeanMode =
                pad(
                    (strVal =
                        Utils.doubleToString(m_ClusterCentroids.instance(j).value(i),
                            maxWidth, 4).trim()), " ", maxWidth + 1 - strVal.length(),
                    true);
          }
        }
        temp.append(valMeanMode);
      }
      temp.append("\n");

      if (m_displayStdDevs) {
        // Std devs/max nominal
        String stdDevVal = "";

        if (m_ClusterCentroids.attribute(i).isNominal()) {
          // Do the values of the nominal attribute
          Attribute a = m_ClusterCentroids.attribute(i);
          for (int j = 0; j < a.numValues(); j++) {
            // full data
            String val = "  " + a.value(j);
            temp.append(pad(val, " ", maxAttWidth + 1 - val.length(), false));
            double count = m_FullNominalCounts[i][j];
            int percent =
                (int) ((double) m_FullNominalCounts[i][j]
                    / Utils.sum(m_ClusterSizes) * 100.0);
            String percentS = "" + percent + "%)";
            percentS = pad(percentS, " ", 5 - percentS.length(), true);
            stdDevVal = "" + count + " (" + percentS;
            stdDevVal =
                pad(stdDevVal, " ", maxWidth + 1 - stdDevVal.length(), true);
            temp.append(stdDevVal);

            // Clusters
            for (int k = 0; k < m_NumClusters; k++) {
              percent =
                  (int) ((double) m_ClusterNominalCounts[k][i][j]
                      / m_ClusterSizes[k] * 100.0);
              percentS = "" + percent + "%)";
              percentS = pad(percentS, " ", 5 - percentS.length(), true);
              stdDevVal = "" + m_ClusterNominalCounts[k][i][j] + " (" + percentS;
              stdDevVal =
                  pad(stdDevVal, " ", maxWidth + 1 - stdDevVal.length(), true);
              temp.append(stdDevVal);
            }
            temp.append("\n");
          }
          // missing (if any)
          if (m_FullMissingCounts[i] > 0) {
            // Full data
            temp.append(pad("  missing", " ",
                maxAttWidth + 1 - "  missing".length(), false));
            double count = m_FullMissingCounts[i];
            int percent =
                (int) ((double) m_FullMissingCounts[i]
                    / Utils.sum(m_ClusterSizes) * 100.0);
            String percentS = "" + percent + "%)";
            percentS = pad(percentS, " ", 5 - percentS.length(), true);
            stdDevVal = "" + count + " (" + percentS;
            stdDevVal =
                pad(stdDevVal, " ", maxWidth + 1 - stdDevVal.length(), true);
            temp.append(stdDevVal);

            // Clusters
            for (int k = 0; k < m_NumClusters; k++) {
              percent =
                  (int) ((double) m_ClusterMissingCounts[k][i]
                      / m_ClusterSizes[k] * 100.0);
              percentS = "" + percent + "%)";
              percentS = pad(percentS, " ", 5 - percentS.length(), true);
              stdDevVal = "" + m_ClusterMissingCounts[k][i] + " (" + percentS;
              stdDevVal =
                  pad(stdDevVal, " ", maxWidth + 1 - stdDevVal.length(), true);
              temp.append(stdDevVal);
            }

            temp.append("\n");
          }

          temp.append("\n");
        } else {
          // Full data
          if (Double.isNaN(m_FullMeansOrMediansOrModes[i])) {
            stdDevVal = pad("--", " ", maxAttWidth + maxWidth + 1 - 2, true);
          } else {
            stdDevVal =
                pad(
                    (strVal =
                        plusMinus
                            + Utils.doubleToString(m_FullStdDevs[i], maxWidth, 4)
                            .trim()), " ",
                    maxWidth + maxAttWidth + 1 - strVal.length(), true);
          }
          temp.append(stdDevVal);

          // Clusters
          for (int j = 0; j < m_NumClusters; j++) {
            if (m_ClusterCentroids.instance(j).isMissing(i)) {
              stdDevVal = pad("--", " ", maxWidth + 1 - 2, true);
            } else {
              stdDevVal =
                  pad(
                      (strVal =
                          plusMinus
                              + Utils.doubleToString(
                              m_ClusterStdDevs.instance(j).value(i), maxWidth, 4)
                              .trim()), " ", maxWidth + 1 - strVal.length(), true);
            }
            temp.append(stdDevVal);
          }
          temp.append("\n\n");
        }
      }
    }

    temp.append("\n\n");

    if (useMeasures && okmStringResults.length() > 0) {
      temp.append("=========== Measures ==========\n");
      temp.append(okmStringResults);
      temp.append("\n");
    }
    return temp.toString();
  }

  private String pad(String source, String padChar, int length, boolean leftPad) {
    StringBuffer temp = new StringBuffer();

    if (leftPad) {
      for (int i = 0; i < length; i++) {
        temp.append(padChar);
      }
      temp.append(source);
    } else {
      temp.append(source);
      for (int i = 0; i < length; i++) {
        temp.append(padChar);
      }
    }
    return temp.toString();
  }

  /**
   * Gets the the cluster centroids.
   *
   * @return the cluster centroids
   */
  public Instances getClusterCentroids() {
    return m_ClusterCentroids;
  }

  /**
   * Gets the standard deviations of the numeric attributes in each cluster.
   *
   * @return the standard deviations of the numeric attributes in each cluster
   */
  public Instances getClusterStandardDevs() {
    return m_ClusterStdDevs;
  }

  /**
   * Returns for each cluster the weighted frequency counts for the values of each
   * nominal attribute.
   *
   * @return the counts
   */
  public double[][][] getClusterNominalCounts() {
    return m_ClusterNominalCounts;
  }

  /**
   * Gets the squared error for all clusters.
   *
   * @return the squared error, NaN if fast distance calculation is used
   * @see #m_FastDistanceCalc
   */
//  public double getSquaredError() {
//    if (m_FastDistanceCalc) {
//      return Double.NaN;
//    } else {
//      return Utils.sum(m_squaredErrors);
//    }
//  }

  /**
   * Gets the sum of weights for all the instances in each cluster.
   *
   * @return The number of instances in each cluster
   */
  public double[] getClusterSizes() {
    return m_ClusterSizes;
  }

  /**
   * Gets the assignments for each instance.
   *
   * @return Array of indexes of the centroid assigned to each instance
   * @throws Exception if order of instances wasn't preserved or no assignments
   *                   were made
   */
  public int[] getAssignments() throws Exception {
    if (m_Assignments == null) {
      throw new Exception("No assignments made.");
    }
    return m_Assignments;
  }

  /**
   * Returns the revision string.
   *
   * @return the revision
   */
  @Override
  public String getRevision() {
    return RevisionUtils.extract("$Revision: 11444 $");
  }

  /**
   * Main method for executing this class.
   *
   * @param args use -h to list all parameters
   */
  public static void main(String[] args) {
    runClusterer(new SimpleKMeans(), args);
  }

  private class InitClusters {
    //    private Instances data;
    private Instances instances;
    private int[] clusterAssignments;

    public InitClusters() {
//      this.data = data;
    }

    public Instances getInstances() {
      return instances;
    }

    public int[] getClusterAssignments() {
      return clusterAssignments;
    }

    public InitClusters invoke(Instances data) throws Exception {

      // can clusterer handle the data?
      getCapabilities().testWithFail(data);


      instances = new Instances(data);

      instances.setClassIndex(-1);

      m_ClusterNominalCounts = new double[m_NumClusters][instances.numAttributes()][];
      m_ClusterMissingCounts = new double[m_NumClusters][instances.numAttributes()];
      if (m_displayStdDevs) {
        m_FullStdDevs = instances.variances();
      }

      m_FullMeansOrMediansOrModes = moveCentroid(0, instances, true, false);

      m_FullMissingCounts = m_ClusterMissingCounts[0];
      m_FullNominalCounts = m_ClusterNominalCounts[0];
      double sumOfWeights = instances.sumOfWeights();
      for (int i = 0; i < instances.numAttributes(); i++) {
        if (instances.attribute(i).isNumeric()) {
          if (m_displayStdDevs) {
            m_FullStdDevs[i] = Math.sqrt(m_FullStdDevs[i]);
          }
          if (m_FullMissingCounts[i] == sumOfWeights) {
            m_FullMeansOrMediansOrModes[i] = Double.NaN; // mark missing as mean
          }
        } else {
          if (m_FullMissingCounts[i] > m_FullNominalCounts[i][Utils
              .maxIndex(m_FullNominalCounts[i])]) {
            m_FullMeansOrMediansOrModes[i] = -1; // mark missing as most common
            // value
          }
        }
      }

      m_ClusterCentroids = new Instances(instances, m_NumClusters);
      clusterAssignments = new int[instances.numInstances()];

      m_Assignments = clusterAssignments;

      m_DistanceFunction.setInstances(instances);

      Random RandomO = new Random(getSeed());
      int instIndex;
      HashMap<DecisionTableHashKey, Integer> initC =
          new HashMap<DecisionTableHashKey, Integer>();
      DecisionTableHashKey hk = null;

      Instances initInstances = null;
      initInstances = new Instances(instances);


      // random
      for (int j = initInstances.numInstances() - 1; j >= 0; j--) {
        instIndex = RandomO.nextInt(j + 1);
        hk =
            new DecisionTableHashKey(initInstances.instance(instIndex),
                initInstances.numAttributes(), true);
        if (!initC.containsKey(hk)) {
          m_ClusterCentroids.add(initInstances.instance(instIndex));
          initC.put(hk, null);
        }
        initInstances.swap(j, instIndex);

        if (m_ClusterCentroids.numInstances() == m_NumClusters) {
          break;
        }
      }

      m_initialStartPoints = BUtils.sample(data, m_NumClusters, m_Seed);
      m_ClusterCentroids = new Instances(m_initialStartPoints);
//      m_NumClusters = m_ClusterCentroids.numInstances();


      // removing reference
      initInstances = null;
      return this;
    }
  }

}

