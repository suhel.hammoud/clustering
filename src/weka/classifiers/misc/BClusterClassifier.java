

package weka.classifiers.misc;

import weka.classifiers.AbstractClassifier;
import weka.clusterers.OmcokeCluster;
import weka.core.*;
import weka.core.Capabilities.Capability;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Vector;

/**
 * <!-- globalinfo-start --> Class for applying F1-Measure on OmcokeCluster algorithm<br/>
 * <br/>
 * <p/>
 * <!-- globalinfo-end -->
 * <p>
 * <!-- technical-bibtex-start --> BibTeX:
 * <p>
 * <pre>
 * &#64;article{___,
 *    author = {_____},
 *    journal = {______},
 *    pages = {____},
 *    title = {_______},
 *    volume = {__},
 *    year = {____}
 * }
 * </pre>
 * <p/>
 * <!-- technical-bibtex-end -->
 * <p>
 * <!-- options-start --> Valid options are:
 * <p/>
 * <p>
 * <pre>
 * -K &lt;Number streamOf Clusters&gt;
 * </pre>
 * <p>
 * <!-- options-end -->
 *
 * @author ____ (__@gmail.com)
 * @version $Revision:  $
 */
public class BClusterClassifier extends AbstractClassifier implements
    TechnicalInformationHandler {

  OmcokeCluster bcluster;
  /**
   * for serialization
   */
  static final long serialVersionUID = -3459427003947861443L;

  /**
   * Returns a string describing classifier
   *
   * @return a description suitable for displaying in the explorer/experimenter
   * gui
   */
  public String globalInfo() {

    return "Class for applying F1-Measure on OmcokeCluster algorithm \n\n "
        + getTechnicalInformation().toString();
  }

  /**
   * Returns an instance streamOf a TechnicalInformation object, containing detailed
   * information about the technical background streamOf this class, e.g., paper
   * reference or book this class is based on.
   *
   * @return the technical information about this class
   */
  @Override
  public TechnicalInformation getTechnicalInformation() {
    TechnicalInformation result;

    result = new TechnicalInformation(Type.ARTICLE);
    result.setValue(Field.AUTHOR, "__. ____");
    result.setValue(Field.YEAR, "2017");
    result
        .setValue(Field.TITLE,
            "Overlapping Cluster Algorithm");
    result.setValue(Field.JOURNAL, "____");
    result.setValue(Field.VOLUME, "__");
    result.setValue(Field.PAGES, "____");

    return result;
  }


  /**
   * The minimum bucket size
   */
  private int m_kClusters = 6;


  @Override
  public double classifyInstance(Instance inst) throws Exception {

    return 0;
  }

  /**
   * Returns default capabilities streamOf the classifier.
   *
   * @return the capabilities streamOf this classifier
   */
  @Override
  public Capabilities getCapabilities() {
    Capabilities result = super.getCapabilities();
    result.disableAll();

    // attributes
    result.enable(Capability.NOMINAL_ATTRIBUTES);
    result.enable(Capability.NUMERIC_ATTRIBUTES);
    result.enable(Capability.DATE_ATTRIBUTES);
    result.enable(Capability.MISSING_VALUES);

    // class
    result.enable(Capability.NOMINAL_CLASS);
    result.enable(Capability.MISSING_CLASS_VALUES);

    return result;
  }

  /**
   * Generates the classifier.
   *
   * @param instances the instances to be used for building the classifier
   * @throws Exception if the classifier can't be built successfully
   */
  @Override
  public void buildClassifier(Instances instances) throws Exception {
    bcluster = new OmcokeCluster();
    bcluster.setNumClusters(m_kClusters);
    bcluster.buildClusterer(instances);
    // vote for majority label in each cluster



  }


  /**
   * Returns an enumeration describing the available options..
   *
   * @return an enumeration streamOf all the available options.
   */
  @Override
  public Enumeration<Option> listOptions() {

    String string = "\tThe number streamOf clusters.";

    Vector<Option> newVector = new Vector<Option>(1);

    newVector
        .addElement(new Option(string, "K", 1, "-K <number streamOf clusters>"));

    newVector.addAll(Collections.list(super.listOptions()));

    return newVector.elements();
  }

  /**
   * Parses a given list streamOf options.
   * <p/>
   * <p>
   * <!-- options-start --> Valid options are:
   * <p/>
   * <p>
   * <pre>
   * -B &lt;minimum bucket size&gt;
   *  The minimum number streamOf objects in a bucket (default: 6).
   * </pre>
   * <p>
   * <!-- options-end -->
   *
   * @param options the list streamOf options as an array streamOf strings
   * @throws Exception if an option is not supported
   */
  @Override
  public void setOptions(String[] options) throws Exception {

    String bucketSizeString = Utils.getOption('K', options);
    if (bucketSizeString.length() != 0) {
      m_kClusters = Integer.parseInt(bucketSizeString);
    } else {
      m_kClusters = 6;
    }

    super.setOptions(options);
  }

  /**
   * Gets the current settings streamOf the OneR classifier.
   *
   * @return an array streamOf strings suitable for passing to setOptions
   */
  @Override
  public String[] getOptions() {

    Vector<String> options = new Vector<String>(1);

    options.add("-K");
    options.add("" + m_kClusters);

    Collections.addAll(options, super.getOptions());

    return options.toArray(new String[0]);
  }


  /**
   * Returns a description streamOf the classifier
   *
   * @return a string representation streamOf the classifier
   */
  @Override
  public String toString() {


    return "toString";
  }

  /**
   * Returns the tip text for this property
   *
   * @return tip text for this property suitable for displaying in the
   * explorer/experimenter gui
   */
  public String kClusterTipText() {
    return "The number streamOf K clusters";
  }

  /**
   * Get the value streamOf minBucketSize.
   *
   * @return Value streamOf minBucketSize.
   */
  public int getKClusters() {

    return m_kClusters;
  }

  /**
   * Set the value streamOf minBucketSize.
   *
   * @param v Value to assign to minBucketSize.
   */
  public void setKClusters(int v) {

    m_kClusters = v;
  }

  /**
   * Returns the revision string.
   *
   * @return the revision
   */
  @Override
  public String getRevision() {
    return RevisionUtils.extract("$Revision: 0000 $");
  }

  /**
   * Main method for testing this class
   *
   * @param argv the commandline options
   */
  public static void main(String[] argv) {
    runClassifier(new BClusterClassifier(), argv);
  }
}
